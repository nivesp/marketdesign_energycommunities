''' 
=========================================================================
Script         : Generates and solves opt problems
Author         : Niklas Vepermann
Project        : Risk trading in energy communities
=========================================================================
'''
import pandas as pd    # Pandas
import numpy as np
import time
from datetime import datetime
import matplotlib.pyplot as plt
import tikzplotlib

from collections import OrderedDict 

from OptProblem import OptProblem
from func.PVScenarios import gen_PVInput
from func.PVScenarios import clustered_PV

# load model settings
exec(open('./data/input/ModelSettings.py').read())

# generate PV power scenarios
PV_mean =	[0.0, 0.0, 0.0, 0.0, 0.01, 0.1, 0.2, 0.3, 0.5,
			0.6, 0.8, 0.85, 0.8, 0.8, 0.7, 0.6, 0.5, 0.3,
			0.2, 0.1, 0.01, 0.0, 0.0, 0.0] # Expected PV power generation			
Number_Samples = 1000 # Sampling scenarios
var = 0.025

from func.PVScenarios import gen_PVScenarios
FullSample_of_PV_AUX = gen_PVScenarios(PV_mean,Number_Samples,var) # generate scenarios

spot_var_lb = pd.DataFrame()
spot_var_ub = pd.DataFrame()
spot_payoff = pd.DataFrame()

List_of_Scenarios = []	
Number_of_Scenarios = 750
FullSample_of_PV = clustered_PV(FullSample_of_PV_AUX,Number_of_Scenarios)


for NumberScenarios in list(range(1,Number_of_Scenarios+1,1)):

	
	# load data
	exec(open('./data/input/InputData_case1.py').read())

	########
	# SPOT #
	########

	model_name = 'spot'
	PV_scenarios = FullSample_of_PV[0:NumberScenarios]
	#PV_scenarios = clustered_PV(FullSample_of_PV_AUX,NumberScenarios)
	Weight,Scenarios,PowerPV = gen_PVInput(NumberScenarios,PV_scenarios,Players,Time)

	spot_Input = 	{'Players' : Players,
					 'Scenarios' : Scenarios,
					 'Time' : Time,
					 'PriceBuy' : PriceBuy,
					 'PriceSell' : PriceSell,
					 'Weight' : Weight,
					 'PowerPV' : PowerPV,
					 'Demand' : Demand,
					 'Storage' : Storage,
					 'StorageLoc' : StorageLoc,
					 'EffCh' : EffCh,
					 'EffDis' : EffDis,
					 'SocIni' : SocIni,
					 'ChMax' : ChMax,
					 'DisMax' : DisMax,
					 'SocMax' : SocMax}  

	# Initialize model and run Optimization							
	spot = OptProblem(model_name,spot_Input,spot_Settings) 
	spot.run_opt()
	spot.get_results(spot_Settings)

	# Collect results
	#################
	spot_var_lb = spot_var_lb.append(pd.DataFrame( spot.Payoff_per_scenario.mean() - spot.Payoff_per_scenario.std()).transpose(), ignore_index=True)
	spot_var_ub = spot_var_ub.append(pd.DataFrame( spot.Payoff_per_scenario.mean() + spot.Payoff_per_scenario.std()).transpose(),  ignore_index=True)
	spot_payoff = spot_payoff.append(spot.Payoff_per_scenario.mean(), ignore_index=True)


	# Wasserstein distance
	flat_list = []
	for sublist in PV_scenarios:
		for item in sublist:
			flat_list.append(item)
		#flat_list.append(sublist[12]) # metering Wasserstein distance at hour 12

	List_of_Scenarios.append(flat_list)


rangeStudy = list(range(1,len(List_of_Scenarios)))	
	
from scipy import stats
WDistance = ['NaN']
for x in rangeStudy:
	WDistance.append(round(stats.wasserstein_distance(List_of_Scenarios[x-1],List_of_Scenarios[x]),3))
	

rangeStudyAUX = list(range(0,len(List_of_Scenarios)))	
	
# Plot results	
fig1 = plt.figure()
ax1 = fig1.add_subplot(111)
#
ax1.plot(rangeStudyAUX,spot_payoff['Tot'],color='black',label='System cost')
ax1.fill_between(rangeStudyAUX,spot_var_lb['Tot'], spot_var_ub['Tot'],
	color='gray',alpha=0.5,label='standard deviation')
ax1.plot(rangeStudyAUX[1],WDistance[1], color='red',label='Wasserstein distance')
ax1.set_ylabel('System cost')
ax1.legend()
ax1.set_xlim(0,max(rangeStudyAUX))
#
ax2 = ax1.twinx()
ax2.plot(rangeStudyAUX,WDistance, color='red')
ax2.set_ylabel('Wasserstein distance')
tikzplotlib.save('./data/output/graphics/%s_Opt_Number_Scenarios_scen%s.tex' 
	% (datetime.today().strftime('%Y%m%d'),str(Number_of_Scenarios)),
		   axis_height = '\\fheight',
		   axis_width = '\\fwidth')
plt.savefig('./data/output/graphics/%s_Opt_Number_Scenarios_scen%s.pdf' 
	% (datetime.today().strftime('%Y%m%d'),str(Number_of_Scenarios)),
	format='pdf', dpi=1000)
