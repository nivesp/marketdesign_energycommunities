''' 
=========================================================================
Script         : Input data
Author         : Niklas Vepermann
Project        : Energy communities and local markets
=========================================================================
'''
from collections import OrderedDict 
from random import uniform

Players = ['Player%d' % x for x in range(1,NumbPlayers+1)]
Number_of_TimeSteps = 24
Time = ['t%d' % x for x in range(1,Number_of_TimeSteps+1)]

# storage system
if NumbPlayers >= 4:
	Storage = ['st1','st2']
	StorageLoc = {n : [] for n in Players}
	StorageLoc.update( {'Player2' : ['st1'], 'Player4' : ['st2']} )

	SocIni = {('st1'): 0,
			 ('st2'): 0}
			 
	EffCh = {('st1'): 0.95,
			('st2'): 0.95}
			
	EffDis = {('st1'): 1.05,
			 ('st2'): 1.05}

	SocMax = {('st1'): 10,
			 ('st2'): 10}

	ChMax = {('st1'): 4.5,
			('st2'): 4.5}

	DisMax = {('st1'): 4.5,
			  ('st2'): 4.5}


if NumbPlayers >= 8:
	Storage.append('st3')
	StorageLoc.update( {'Player8' : ['st3']} )
	SocIni.update( {'st3' : 0} )
	EffCh.update( {'st3' : 0.90} )
	EffDis.update( {'st3' : 1.10} )
	SocMax.update( {'st3' : 10} )
	ChMax.update( {'st3' : 4.5} )
	DisMax.update( {'st3' : 4.5} )
	
if NumbPlayers >= 12:
	Storage.append('st4')
	StorageLoc.update( {'Player12' : ['st4']} )
	SocIni.update( {'st4' : 0} )
	EffCh.update( {'st4' : 0.90} )
	EffDis.update( {'st4' : 1.10} )
	SocMax.update( {'st4' : 10} )
	ChMax.update( {'st4' : 4.5} )
	DisMax.update( {'st4' : 4.5} )	


# in kWh
Demand_AUX = OrderedDict({'t1' : 0.2,
			  't2' : 0.1,
			  't3' : 0.2,
			  't4' : 0.3,
			  't5' : 0.4,
			  't6' : 0.6,
			  't7' : 0.9,
			  't8' : 1.3,
			  't9' : 1.8,
			  't10' : 0.3,
			  't11' : 0.8,
			  't12' : 0.7,
			  't13' : 0.6,
			  't14' : 0.5,
			  't15' : 0.5,
			  't16' : 0.6,
			  't17' : 0.9,
			  't18' : 1.3,
			  't19' : 1.7,
			  't20' : 2.0,
			  't21' : 1.6,
			  't22' : 1.2,
			  't23' : 0.8,
			  't24' : 0.3})

# in Eur/kWh
PriceBuy =   OrderedDict({'t1' : 0.1,
			  't2' : 0.1,
			  't3' : 0.1,
			  't4' : 0.1,
			  't5' : 0.1,
			  't6' : 0.1,
			  't7' : 0.20,
			  't8' : 0.50,
			  't9' : 0.50,
			  't10' : 0.50,
			  't11' : 0.40,
			  't12' : 0.30,
			  't13' : 0.30,
			  't14' : 0.30,
			  't15' : 0.20,
			  't16' : 0.20,
			  't17' : 0.20,
			  't18' : 0.20,
			  't19' : 0.50,
			  't20' : 0.50,
			  't21' : 0.50,
			  't22' : 0.20,
			  't23' : 0.10,
			  't24' : 0.10})

epsilon = 0.3
PriceSell = OrderedDict({key : round((1 - epsilon)*value,2) 
    for key,value in PriceBuy.items()})  

# Demand
Demand = OrderedDict({(player,key) : value for player in Players 
    for key,value in Demand_AUX.items()})

# Player1 demand
Demand.update({('Player1',key[1]) : round(1.0*value,2) 
	for key,value in Demand.items()})
	
# Player2 demand
Demand.update({('Player2',key[1]) : round(1.0*value,2) 
	for key,value in Demand.items()})

# Player3 demand
Demand.update({('Player3',key[1]) : round(1.0*value,2) 
	for key,value in Demand.items()})

# Player4 demand
Demand.update({('Player4',key[1]) : round(1.0*value,2) 
	for key,value in Demand.items()})
	
if 'Player5' in Players:
	Demand.update({('Player5',key[1]) : round(uniform(0.8,1.2)*value,2) 
		for key,value in Demand.items()})

if 'Player6' in Players:
	Demand.update({('Player6',key[1]) : round(uniform(0.8,1.2)*value,2) 
		for key,value in Demand.items()})	

if 'Player7' in Players:
	Demand.update({('Player7',key[1]) : round(uniform(0.8,1.2)*value,2) 
		for key,value in Demand.items()})
		
if 'Player8' in Players:
	Demand.update({('Player8',key[1]) : round(uniform(0.8,1.2)*value,2) 
		for key,value in Demand.items()})
		
if 'Player9' in Players:
	Demand.update({('Player9',key[1]) : round(uniform(0.8,1.2)*value,2) 
		for key,value in Demand.items()})
		
if 'Player10' in Players:
	Demand.update({('Player10',key[1]) : round(uniform(0.8,1.2)*value,2) 
		for key,value in Demand.items()})
		
if 'Player11' in Players:
	Demand.update({('Player11',key[1]) : round(uniform(0.8,1.2)*value,2) 
		for key,value in Demand.items()})
		
if 'Player12' in Players:
	Demand.update({('Player12',key[1]) : round(uniform(0.8,1.2)*value,2) 
		for key,value in Demand.items()})
		
if 'Player13' in Players:
	Demand.update({('Player13',key[1]) : round(uniform(0.8,1.2)*value,2) 
		for key,value in Demand.items()})
		
if 'Player14' in Players:
	Demand.update({('Player14',key[1]) : round(uniform(0.8,1.2)*value,2) 
		for key,value in Demand.items()})
		
if 'Player15' in Players:
	Demand.update({('Player15',key[1]) : round(uniform(0.8,1.2)*value,2) 
		for key,value in Demand.items()})
		
if 'Player16' in Players:
	Demand.update({('Player16',key[1]) : round(uniform(0.8,1.2)*value,2) 
		for key,value in Demand.items()})