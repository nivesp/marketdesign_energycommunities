''' 
=========================================================================
Script         : Model settings
Author         : Niklas Vepermann
Project        : Energy communities and local markets
=========================================================================
'''
########
# SPOT #
########
	
spot_Settings = {'how' :		['spot'],
				'variables' : 	['powerH','powerBuy','powerSell',
								'powerCh','powerDis','soc'],
				'objective' : 	['SocialWelfare'],
				'constraints' : ['EQ_balanceH','EQ_socIni',
								'EQ_soc','EQ_ChMax','EQ_DisMax','EQ_SocMax',
								'EQ_balanceLoc'],
				'results' : 	['objective','powerH','powerCh',
								'powerDis','soc','powerBuy','powerSell',
								'dual_balanceLoc'],
				'eval' :		['OperationPayoffs']}
	

##############
# SPOT + PSR #
##############				
				
spot_PSR_Settings = {'how' :	['spot_PSR'],
				'variables' : 	['powerH','powerBuy','powerSell',
								'powerCh_PSR','powerDis_PSR','soc_PSR',
								'chRiBid','disRiBid','socRiBid',
								'chRi','disRi','socRi'],
				'objective' : 	['SocialWelfarePSR'],
				'constraints' : ['EQ_ChRiMaxBid','EQ_DisRiMaxBid',
								'EQ_SocRiMaxBid',
								'EQ_ChRiMaxOffer','EQ_DisRiMaxOffer',
								'EQ_SocRiMaxOffer',
								'EQ_ChRiBE','EQ_DisRiBE','EQ_SocRiBE',
								#
								'EQ_balance_PSR',
								'EQ_socIni_PSR','EQ_soc_PSR',
								'EQ_powerChMax_PSR','EQ_powerDisMax_PSR',
								'EQ_socMax_PSR',
								'EQ_balanceLoc'],
				'results' : 	['objective',
								'powerBuy','powerSell','powerH',
								'powerCh_PSR','powerDis_PSR','soc_PSR',
								'chRiBid',
								'disRiBid','socRiBid','chRi','disRi','socRi',
								'dual_ChRiBE','dual_DisRiBE','dual_SocRiBE',
								'dual_balanceLoc'],
				'eval' :		['OperationPayoffs','RightPayoffs']}		

	
##############
# SPOT + FSR #
##############	
			
spot_FSR_Settings = {'how' :	['spot_FSR'],
				'variables' : 	['powerH','powerBuy','powerSell',
								'powerCh_FSR','powerDis_FSR','soc_FSR'],
				'objective' : 	['SocialWelfare'],
				'constraints' : ['EQ_balanceH_FSR','EQ_socIni_FSR',
								'EQ_soc_FSR','EQ_ChMax_FSR','EQ_DisMax_FSR',
								'EQ_SocMax_FSR','EQ_balanceLoc_FSR'],
				'results' : 	['objective',
								'powerH','powerBuy','powerSell',
								'powerCh_FSR','powerDis_FSR','soc_FSR',
								'dual_balanceLoc_FSR','dual_ChMax_FSR',
								'dual_DisMax_FSR','dual_SocMax_FSR'],
				'eval' :		['OperationPayoffs']}				
				
frwd_FSR_Settings = {'how' :	['frwd_FSR'],
				'variables' : 	['chRiBid','disRiBid','socRiBid',
								'chRi','disRi','socRi'],
				'objective' : 	['FSRAuction'],
				'constraints' : ['EQ_ChRiMaxBid','EQ_DisRiMaxBid',
								'EQ_SocRiMaxBid',
								'EQ_ChRiMaxOffer','EQ_DisRiMaxOffer',
								'EQ_SocRiMaxOffer',
								'EQ_ChRiBE','EQ_DisRiBE','EQ_SocRiBE'],
				'results' : 	['objective',
								'chRiBid','disRiBid','socRiBid',
								'chRi','disRi','socRi',
								'dual_ChRiBE','dual_DisRiBE','dual_SocRiBE'],
				'eval' :		['RightPayoffs']}	


###############
# Cooperative #
###############
	
Coop_Settings = {'how' :		['spot'],
				'variables' : 	['powerH','powerBuy','powerSell',
								'powerCh','powerDis','soc'],
				'objective' : 	['SocialWelfare'],
				'constraints' : ['EQ_balanceH','EQ_socIni',
								'EQ_soc','EQ_ChMax','EQ_DisMax','EQ_SocMax',
								'EQ_balanceLoc'],
				'results' : 	['objective','powerH','powerCh',
								'powerDis','soc','powerBuy','powerSell',
								'dual_balanceLoc'],
				'eval' :		['OperationPayoffs']}			