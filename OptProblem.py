''' 
=========================================================================
Script         : Opt problems: SP, PSR, FSR
Author         : Niklas Vepermann
Project        : Energy communities and local markets
=========================================================================
''' 

from gurobipy import * # Gurobi
import pandas as pd    # Pandas
from collections import OrderedDict 

class OptProblem():
    def __init__(self,Name,Input,Settings):
        self.m = Model(Name)
        self.create_sets_parameters(Input)
        self.create_variables(Settings)
        self.create_objective(Settings)
        self.create_constraints(Settings)

		
    def run_opt(self):
        self.m.Params.OptimalityTol = 1e-09
        self.m.Params.FeasibilityTol = 1e-09
        self.m.Params.method = 2
        self.m.Params.BarQCPConvTol = 1e-9
        self.m.Params.BarConvTol = 1e-9
        self.m.Params.QCPDual = 1
        self.m.update()
#        self.m.write('%s.lp' % self.m.model_name)
        print(self.m.modelname)
        self.m.optimize()
        if self.m.status == GRB.Status.OPTIMAL:
            print('\n OPTIMAL SOLUTION FOUND \n')
        else:
            sys.exit('\n ERROR: NO SOLUTION FOUND \n')
           

    def create_sets_parameters(self,Input):
	# Sets #
	########
        if 'Players' in Input.keys():
            self.Players = Input['Players']
        if 'Scenarios' in Input.keys():
            self.Scenarios = Input['Scenarios']
        if 'Time' in Input.keys():
            self.Time = Input['Time']
        if 'Storage' in Input.keys():
            self.Storage = Input['Storage']
        if 'StorageLoc' in Input.keys():
            self.StorageLoc = Input['StorageLoc']
			
    # Parameters #
	##############
        if 'PriceBuy' in Input.keys():
            self.PriceBuy = Input['PriceBuy']
        if 'PriceSell' in Input.keys():
            self.PriceSell = Input['PriceSell']
        if 'Weight' in Input.keys():
            self.Weight = Input['Weight']
        if 'PowerPV' in Input.keys():
            self.PowerPV = Input['PowerPV']
        if 'Demand' in Input.keys():
            self.Demand = Input['Demand']
			
        if 'EffCh' in Input.keys():
            self.EffCh = Input['EffCh']
        if 'EffDis' in Input.keys():
            self.EffDis = Input['EffDis']
        if 'SocIni' in Input.keys():
            self.SocIni = Input['SocIni']
        if 'ChMax' in Input.keys():
            self.ChMax = Input['ChMax']
        if 'DisMax' in Input.keys():
            self.DisMax = Input['DisMax']
        if 'SocMax' in Input.keys():
            self.SocMax = Input['SocMax']        

        if 'ValueChRi' in Input.keys():
            self.ValueChRi = Input['ValueChRi']
        if 'ValueDisRi' in Input.keys():
            self.ValueDisRi = Input['ValueDisRi']
        if 'ValueSocRi' in Input.keys():
            self.ValueSocRi = Input['ValueSocRi']
			
    def create_variables(self,Settings):   
    # Social Welfare Maximization     
        if 'powerH' in Settings['variables']:
            self.powerH = self.m.addVars(self.Players,self.Scenarios,self.Time, lb=-GRB.INFINITY, name="powerH")   
        if 'powerBuy' in Settings['variables']:
            self.powerBuy = self.m.addVars(self.Scenarios,self.Time, name="powerBuy")
        if 'powerSell' in Settings['variables']:
            self.powerSell = self.m.addVars(self.Scenarios,self.Time, name="powerSell")
			
        if 'powerCh' in Settings['variables']:
            self.powerCh = self.m.addVars(self.Storage,self.Scenarios,self.Time, name="powerCh")
        if 'powerDis' in Settings['variables']:
            self.powerDis = self.m.addVars(self.Storage,self.Scenarios,self.Time, name="powerDis")
        if 'soc' in Settings['variables']:
            self.soc = self.m.addVars(self.Storage,self.Scenarios,self.Time, name="soc")   

	# PSR Storage operation            
        if 'powerCh_PSR' in Settings['variables']:
            self.powerCh_PSR = self.m.addVars( self.Players, self.Storage, self.Scenarios,self.Time, name="powerCh_PSR")
        if 'powerDis_PSR' in Settings['variables']:
            self.powerDis_PSR = self.m.addVars(	self.Players,self.Storage, self.Scenarios,self.Time, name="powerDis_PSR")
        if 'soc_PSR' in Settings['variables']:
            self.soc_PSR = self.m.addVars( self.Players,self.Storage, self.Scenarios,self.Time,  name="soc_PSR") 
											
	# FSR Storage operation            
        if 'powerCh_FSR' in Settings['variables']:
            self.powerCh_FSR = self.m.addVars(	self.Storage,self.Scenarios,self.Time, name="powerCh_FSR")
        if 'powerDis_FSR' in Settings['variables']:
            self.powerDis_FSR = self.m.addVars(	self.Storage,self.Scenarios,self.Time, name="powerDis_FSR")
        if 'soc_FSR' in Settings['variables']:
            self.soc_FSR = self.m.addVars( self.Storage,self.Scenarios,self.Time, name="soc_FSR") 
											
	# forward market 
        if 'chRiBid' in Settings['variables']:
            self.chRiBid = self.m.addVars(self.Players, self.Storage, name="chRiBid")  
        if 'disRiBid' in Settings['variables']:
            self.disRiBid = self.m.addVars(self.Players, self.Storage, name="disRiBid") 
        if 'socRiBid' in Settings['variables']:
            self.socRiBid = self.m.addVars(self.Players, self.Storage, name="socRiBid")
			
        if 'chRi' in Settings['variables']:
            self.chRi = self.m.addVars(self.Storage, lb=-GRB.INFINITY, ub=0, name="chRi")
        if 'disRi' in Settings['variables']:
            self.disRi = self.m.addVars(self.Storage, lb=-GRB.INFINITY, ub=0, name="disRi")
        if 'socRi' in Settings['variables']:
            self.socRi = self.m.addVars(self.Storage, lb=-GRB.INFINITY, ub=0, name="socRi")

                       
    def create_objective(self,Settings):      
        if 'SocialWelfare' in Settings['objective']:  
            self.Obj = self.m.setObjective(
				self.Weight*quicksum( self.PriceBuy[t]*self.powerBuy[e,t] - self.PriceSell[t]*self.powerSell[e,t]
									+ quicksum( self.powerH[n,e,t]*self.powerH[n,e,t]*0.00000001 for n in self.Players ) 
									for e in self.Scenarios 
									for t in self.Time ), GRB.MINIMIZE )

        if 'SocialWelfarePSR' in Settings['objective']:  
            self.Obj = self.m.setObjective(
				self.Weight*quicksum( self.PriceBuy[t]*self.powerBuy[e,t] - self.PriceSell[t]*self.powerSell[e,t] 
									+ quicksum( self.powerH[n,e,t]*self.powerH[n,e,t]*0.00000001
									#
									+ quicksum(
										self.chRiBid[n,s]*self.chRiBid[n,s]*0.00000001
										+ self.disRiBid[n,s]*self.disRiBid[n,s]*0.00000001
										+ self.socRiBid[n,s]*self.socRiBid[n,s]*0.00000001
									for s in self.Storage )
										#
									for n in self.Players )
									for e in self.Scenarios 
									for t in self.Time ), GRB.MINIMIZE )

        if 'FSRAuction' in Settings['objective']:  
            self.Obj = self.m.setObjective(
				 - self.Weight*quicksum( quicksum(
										self.ValueChRi[s,e,t]*self.chRiBid[n,s]
										+ self.ValueDisRi[s,e,t]*self.disRiBid[n,s]
										+ self.ValueSocRi[s,e,t]*self.socRiBid[n,s]
										for s in self.Storage )
										#
										+ quicksum(
											self.chRiBid[n,s]*self.chRiBid[n,s]*0.00000001
											+ self.disRiBid[n,s]*self.disRiBid[n,s]*0.00000001
											+ self.socRiBid[n,s]*self.socRiBid[n,s]*0.00000001
										for s in self.Storage )
										#
										for n in self.Players 
										for e in self.Scenarios 
										for t in self.Time ), GRB.MINIMIZE )


    def create_constraints(self,Settings):  
		### Players ###
		###############
		
        # Power balance for individual players
        if 'EQ_balanceH' in Settings['constraints']:
            self.EQ_balanceH = self.m.addConstrs(
                ( self.powerH[n,e,t] + self.PowerPV[n,e,t] + quicksum( - self.powerCh[s,e,t] + self.powerDis[s,e,t] for s in self.StorageLoc[n] ) - self.Demand[n,t] == 0 
                for n in self.Players 
				for e in self.Scenarios 
				for t in self.Time ), name="balanceH" )

        # PSR power balance for individual players
        if 'EQ_balance_PSR' in Settings['constraints']:
            self.EQ_balance_PSR = self.m.addConstrs(
                ( self.powerH[n,e,t] + self.PowerPV[n,e,t] + quicksum( - self.powerCh_PSR[n,s,e,t] + self.powerDis_PSR[n,s,e,t] for s in self.Storage ) - self.Demand[n,t] == 0 
                for n in self.Players 
				for e in self.Scenarios 
				for t in self.Time ), name="balance_PSR" )
				
        # FSR power balance for individual players    
        if 'EQ_balanceH_FSR' in Settings['constraints']:
            self.EQ_balanceH_FSR = self.m.addConstrs(
                ( self.powerH[n,e,t] + self.PowerPV[n,e,t] - self.Demand[n,t] == 0 
                    for n in self.Players 
					for e in self.Scenarios 
					for t in self.Time ), name="balanceH_FSR")
 
        # energy storage system
		#######################
        if 'EQ_socIni' in Settings['constraints']:
            self.EQ_socIni = self.m.addConstrs(
               ( self.soc[s,e,t] == self.SocIni[s] 
			   + self.EffCh[s]*self.powerCh[s,e,t] 
			   - self.EffDis[s]*self.powerDis[s,e,t]
                    for s in self.Storage 
					for e in self.Scenarios 
					for t in self.Time[0:1] ), name="socIni")
        
        if 'EQ_soc' in Settings['constraints']:
            self.EQ_soc = self.m.addConstrs(
                ( self.soc[s,e,t] == 
				self.soc[s,e,self.Time[self.Time.index(t)-1]] 
				+ self.EffCh[s]*self.powerCh[s,e,t] 
				- self.EffDis[s]*self.powerDis[s,e,t]
                    for s in self.Storage 
					for e in self.Scenarios 
					for t in self.Time[1:len(self.Time)] ), name="soc")
 
        if 'EQ_ChMax' in Settings['constraints']:       
            self.EQ_ChMax = self.m.addConstrs(
                ( self.powerCh[s,e,t] <= self.ChMax[s] 
                    for s in self.Storage 
					for e in self.Scenarios 
					for t in self.Time ), name="ChMax")

        if 'EQ_DisMax' in Settings['constraints']:        
            self.EQ_DisMax = self.m.addConstrs(
                ( self.powerDis[s,e,t] <= self.DisMax[s] 
                    for s in self.Storage 
					for e in self.Scenarios 
					for t in self.Time ), name="DisMax")

        if 'EQ_SocMax' in Settings['constraints']:        
            self.EQ_SocMax = self.m.addConstrs(
                ( self.soc[s,e,t] <= self.SocMax[s] 
                    for s in self.Storage 
					for e in self.Scenarios 
					for t in self.Time ), name="SocMax")

					
		# PSR storage constraints
		#########################
        if 'EQ_socIni_PSR' in Settings['constraints']:
            self.EQ_socIni_PSR = self.m.addConstrs(
               ( self.soc_PSR[n,s,e,t] == self.SocIni[s] + self.EffCh[s]*self.powerCh_PSR[n,s,e,t] - self.EffDis[s]*self.powerDis_PSR[n,s,e,t]
					for n in self.Players 
                    for s in self.Storage 
					for e in self.Scenarios 
					for t in self.Time[0:1] ), name="socIni_PSR")
        
        if 'EQ_soc_PSR' in Settings['constraints']:
            self.EQ_soc_PSR = self.m.addConstrs(
                ( self.soc_PSR[n,s,e,t] == self.soc_PSR[n,s,e,self.Time[self.Time.index(t)-1]] + self.EffCh[s]*self.powerCh_PSR[n,s,e,t] - self.EffDis[s]*self.powerDis_PSR[n,s,e,t]
					for n in self.Players 
                    for s in self.Storage 
					for e in self.Scenarios 
					for t in self.Time[1:len(self.Time)] ), name="soc_PSR")
					
        if 'EQ_powerChMax_PSR' in Settings['constraints']:
            self.EQ_powerChMax_PSR = self.m.addConstrs(
                ( self.powerCh_PSR[n,s,e,t] <= self.chRiBid[n,s] 
					for n in self.Players 
                    for s in self.Storage 
					for e in self.Scenarios 
					for t in self.Time ), name="powerChMax_PSR")
            
        if 'EQ_powerDisMax_PSR' in Settings['constraints']:
            self.EQ_powerDisMax_PSR = self.m.addConstrs(
                ( self.powerDis_PSR[n,s,e,t] <= self.disRiBid[n,s] 
					for n in self.Players 
                    for s in self.Storage 
					for e in self.Scenarios 
					for t in self.Time ), name="powerDisMax_PSR")
            
        if 'EQ_socMax_PSR' in Settings['constraints']:
            self.EQ_socMax_PSR = self.m.addConstrs(
                ( self.soc_PSR[n,s,e,t] <= self.socRiBid[n,s] 
					for n in self.Players 
                    for s in self.Storage 
					for e in self.Scenarios 
					for t in self.Time ), name="socMax_PSR")


        # FSR storage constraints
		#########################
        if 'EQ_socIni_FSR' in Settings['constraints']:
            self.EQ_socIni_FSR = self.m.addConstrs(
               ( self.soc_FSR[s,e,t] == self.SocIni[s] + self.EffCh[s]*self.powerCh_FSR[s,e,t] - self.EffDis[s]*self.powerDis_FSR[s,e,t]
					for s in self.Storage 
                    for e in self.Scenarios 
					for t in self.Time[0:1] ), name="socIni_FSR_FSR")
        
        if 'EQ_soc_FSR' in Settings['constraints']:
            self.EQ_soc_FSR = self.m.addConstrs(
                ( self.soc_FSR[s,e,t] == self.soc_FSR[s,e,self.Time[self.Time.index(t)-1]] + self.EffCh[s]*self.powerCh_FSR[s,e,t] - self.EffDis[s]*self.powerDis_FSR[s,e,t]
					for s in self.Storage 
                    for e in self.Scenarios 
			        for t in self.Time[1:len(self.Time)] ), name="soc_FSR")
 
        if 'EQ_ChMax_FSR' in Settings['constraints']:       
            self.EQ_ChMax_FSR = self.m.addConstrs(
                ( self.powerCh_FSR[s,e,t] <= self.ChMax[s]
					for s in self.Storage 
                    for e in self.Scenarios 
					for t in self.Time ), name="ChMax_FSR")

        if 'EQ_DisMax_FSR' in Settings['constraints']:        
            self.EQ_DisMax_FSR = self.m.addConstrs(
                ( self.powerDis_FSR[s,e,t] <= self.DisMax[s]
					for s in self.Storage 
                    for e in self.Scenarios 
					for t in self.Time ), name="DisMax_FSR")

        if 'EQ_SocMax_FSR' in Settings['constraints']:        
            self.EQ_SocMax_FSR = self.m.addConstrs(
                ( self.soc_FSR[s,e,t] <= self.SocMax[s]
					for s in self.Storage 
                    for e in self.Scenarios 
					for t in self.Time ), name="SocMax_FSR")


        ### ENERGY COMMUNITY MANAGER ### 
		################################
		
        # Spot, PSR power balance of energy community
        if 'EQ_balanceLoc' in Settings['constraints']:
            self.EQ_balanceLoc = self.m.addConstrs(
                ( - quicksum( self.powerH[n,e,t] for n in self.Players ) + self.powerBuy[e,t] - self.powerSell[e,t] == 0 
                    for e in self.Scenarios 
					for t in self.Time ), name="balanceLoc") 

        # FSR power balance of energy community
        if 'EQ_balanceLoc_FSR' in Settings['constraints']:
            self.EQ_balanceLoc_FSR = self.m.addConstrs(
                ( - quicksum(self.powerH[n,e,t] for n in self.Players) + self.powerBuy[e,t] - self.powerSell[e,t] 
					+ quicksum( - self.powerCh_FSR[s,e,t] + self.powerDis_FSR[s,e,t] for s in self.Storage ) == 0 
                    for e in self.Scenarios 
					for t in self.Time), name="balanceLoc_FSR")            
            
			
        ### STORAGE RIGHT AUCTION ###
		#############################

        # Offer Limit for Rights
        if 'EQ_ChRiMaxOffer' in Settings['constraints']:
            self.EQ_ChRiMaxOffer = self.m.addConstrs(
                ( ( - self.ChMax[s] <= self.chRi[s] )
					for s in self.Storage), name="ChRiMaxOffer")
            
        if 'EQ_DisRiMaxOffer' in Settings['constraints']:
            self.EQ_DisRiMaxOffer = self. m.addConstrs(
                ( ( - self.DisMax[s] <= self.disRi[s] )
					for s in self.Storage), name="DisRiMaxOffer")
            
        if 'EQ_SocRiMaxOffer' in Settings['constraints']:
            self.EQ_SocRiMaxOffer = self.m.addConstrs(
                ( ( - self.SocMax[s] <= self.socRi[s] )
					for s in self.Storage), name="SocRiMaxOffer")
				
        # Bid Limit for Rights
        if 'EQ_ChRiMaxBid' in Settings['constraints']:
            self.EQ_ChRiMaxBid = self.m.addConstrs(
                ( self.chRiBid[n,s]  <= GRB.INFINITY
					for n in self.Players
					for s in self.Storage ), name="ChRiMaxBid")
            
        if 'EQ_DisRiMaxBid' in Settings['constraints']:
            self.EQ_DisRiMaxBid = self.m.addConstrs(
                ( self.disRiBid[n,s] <= GRB.INFINITY
					for n in self.Players
					for s in self.Storage ), name="DisRiMaxBid")
            
        if 'EQ_SocRiMaxBid' in Settings['constraints']:
            self.EQ_SocRiMaxBid = self.m.addConstrs(
                ( self.socRiBid[n,s]  <= GRB.INFINITY
					for n in self.Players
					for s in self.Storage ), name="SocRiMaxBid")
                       
        # Right balance
        if 'EQ_ChRiBE' in Settings['constraints']:
            self.EQ_ChRiBE = self.m.addConstrs(
                ( self.chRi[s] + quicksum( self.chRiBid[n,s] for n in self.Players ) == 0
					for s in self.Storage ), name="ChRiBE")
            
        if 'EQ_DisRiBE' in Settings['constraints']:
            self.EQ_DisRiBE = self.m.addConstrs(
                ( self.disRi[s] + quicksum( self.disRiBid[n,s] for n in self.Players ) == 0
					for s in self.Storage ), name="DisRiBE")
            
        if 'EQ_SocRiBE' in Settings['constraints']:
            self.EQ_SocRiBE = self.m.addConstrs(
                ( self.socRi[s] + quicksum( self.socRiBid[n,s] for n in self.Players ) == 0
					for s in self.Storage ), name="SocRiBE")
            
            
    def get_results(self,Settings):
        if 'objective' in Settings['results']:
            self.objective = self.m.objVal

        ### Primal Variables ###
        # Social Welfare Maximization
        if 'powerH' in Settings['results']:
            self.powerH = self.m.getAttr('x',self.powerH)               
        if 'powerBuy' in Settings['results']:    
            self.powerBuy = self.m.getAttr('x',self.powerBuy)     
        if 'powerSell' in Settings['results']:
            self.powerSell = self.m.getAttr('x',self.powerSell) 
			
		# storage operation
        if 'powerCh' in Settings['results']:  
            self.powerCh = self.m.getAttr('x',self.powerCh)
        if 'powerDis' in Settings['results']:
            self.powerDis = self.m.getAttr('x',self.powerDis)
        if 'soc' in Settings['results']:
            self.soc = self.m.getAttr('x',self.soc) 

        # PSR storage operation
        if 'powerCh_PSR' in Settings['results']:  
            self.powerCh_PSR = self.m.getAttr('x',self.powerCh_PSR)
        if 'powerDis_PSR' in Settings['results']:
            self.powerDis_PSR = self.m.getAttr('x',self.powerDis_PSR)
        if 'soc_PSR' in Settings['results']:
            self.soc_PSR = self.m.getAttr('x',self.soc_PSR)
			
        # FSR storage operation
        if 'powerCh_FSR' in Settings['results']:  
            self.powerCh_FSR = self.m.getAttr('x',self.powerCh_FSR)
        if 'powerDis_FSR' in Settings['results']:
            self.powerDis_FSR = self.m.getAttr('x',self.powerDis_FSR)
        if 'soc_FSR' in Settings['results']:
            self.soc_FSR = self.m.getAttr('x',self.soc_FSR)
			
        ## Right Auction ##
        if 'chRiBid' in Settings['results']:
            self.chRiBid = self.m.getAttr('x',self.chRiBid)
        if 'disRiBid' in Settings['results']:
            self.disRiBid = self.m.getAttr('x',self.disRiBid)
        if 'socRiBid' in Settings['results']:
            self.socRiBid = self.m.getAttr('x',self.socRiBid)
        if 'chRi' in Settings['results']:            
            self.chRi = self.m.getAttr('x',self.chRi)
        if 'disRi' in Settings['results']:
            self.disRi = self.m.getAttr('x',self.disRi)
        if 'socRi' in Settings['results']:
            self.socRi = self.m.getAttr('x',self.socRi)
           
        ### Dual variables ###    
        if 'dual_balanceLoc' in Settings['results']:
            self.dual_balanceLoc = {key: (1/self.Weight)*value for key,value in self.m.getAttr('pi', self.EQ_balanceLoc).items()}    
        if 'dual_balanceLoc_FSR' in Settings['results']:
            self.dual_balanceLoc_FSR = {key: (1/self.Weight)*value for key,value in self.m.getAttr('pi', self.EQ_balanceLoc_FSR).items()}               
        if 'dual_ChMax' in Settings['results']:
            self.dual_ChMax = self.m.getAttr('pi',self.EQ_ChMax)
        if 'dual_DisMax' in Settings['results']:
            self.dual_DisMax = self.m.getAttr('pi',self.EQ_DisMax)
        if 'dual_SocMax' in Settings['results']:
            self.dual_SocMax = self.m.getAttr('pi',self.EQ_SocMax)

        # FSR storage operation             
        if 'dual_ChMax_FSR' in Settings['results']:
            self.dual_ChMax_FSR = self.m.getAttr('pi',self.EQ_ChMax_FSR)
        if 'dual_DisMax_FSR' in Settings['results']:
            self.dual_DisMax_FSR = self.m.getAttr('pi',self.EQ_DisMax_FSR)
        if 'dual_SocMax_FSR' in Settings['results']:
            self.dual_SocMax_FSR = self.m.getAttr('pi',self.EQ_SocMax_FSR)
			
        # Right Auction #               
        if 'dual_ChRiBE' in Settings['results']:     
            self.dual_ChRiBE = {key: abs(value) for key,value in self.m.getAttr('pi', self.EQ_ChRiBE).items()} 
        if 'dual_DisRiBE' in Settings['results']:
            self.dual_DisRiBE = {key: abs(value) for key,value in self.m.getAttr('pi', self.EQ_DisRiBE).items()} 
        if 'dual_SocRiBE' in Settings['results']:
            self.dual_SocRiBE = {key: abs(value) for key,value in self.m.getAttr('pi', self.EQ_SocRiBE).items()} 
        
        
        ### Cost--Revenue from operation ###
		####################################
        if 'OperationPayoffs' in Settings['eval']:
            if 'spot' in Settings['how']:
                self.price = self.dual_balanceLoc
            if 'spot_PSR' in Settings['how']:
                self.price = self.dual_balanceLoc
            if 'spot_FSR' in Settings['how']:
                self.price = self.dual_balanceLoc_FSR
                
            ### COSTS ###    
            # considering only costs from demand per player
            self.powerHAUX1 = {}			
            for (key,value) in self.powerH.items():
                if value < 0:
                    self.powerHAUX1[key] = 0
                if value >= 0:
                    self.powerHAUX1[key] = self.powerH[key]
					
            # expected cost per player
            self.CostPlayers_expected = {}
            for n in self.Players:
                self.CostPlayers_expected.update(
					{n : self.Weight*( 
						sum( self.powerHAUX1[n,e,t]*self.price[e,t] + self.powerH[n,e,t]*self.powerH[n,e,t]*0.00000001
							for e in self.Scenarios 
							for t in self.Time ) ) })
				
            # scenario specific cost per player
            self.CostPlayers_per_scenario = {}
            for n in self.Players:
                for e in self.Scenarios: 
                    self.CostPlayers_per_scenario.update(
						{(n,e) : ( sum( self.powerHAUX1[n,e,t]*self.price[e,t] + self.powerH[n,e,t]*self.powerH[n,e,t]*0.00000001
							for t in self.Time ) ) } ) 
                    
			# expected revenue for community manager from selling
            self.RevenueCoMa_expected = (
				self.Weight*( sum( sum(-1*self.powerHAUX1[n,e,t]*self.price[e,t] 
					for n in self.Players) 
					- self.PriceSell[t]*self.powerSell[e,t] 
					for e in self.Scenarios 
					for t in self.Time ) ) ) 

			# per scenario revenue for community manager from selling
            self.RevenueCoMa_per_scenario = {}
            for e in self.Scenarios:
                self.RevenueCoMa_per_scenario.update(
					{ e : sum( sum( -1*self.powerHAUX1[n,e,t]*self.price[e,t] 
						for n in self.Players ) 
						- self.PriceSell[t]*self.powerSell[e,t] 
						for t in self.Time ) } ) 
					
			
			### REVENUES ###
            # considering only revenues from supply per player
            self.powerHAUX2 = {}
            for (key,value) in self.powerH.items():
                if value < 0:
                    self.powerHAUX2[key] = self.powerH[key]
                if value >= 0:
                    self.powerHAUX2[key] = 0
					
            # expected revenue per player
            self.RevenuePlayers_expected = {}
            for n in self.Players:
                self.RevenuePlayers_expected.update( 
					{n : self.Weight*( 
							sum( self.powerHAUX2[n,e,t]*self.price[e,t] 
							for e in self.Scenarios 
							for t in self.Time ) ) } )  
				
            # scenario specific revenue per player 
            self.RevenuePlayers_per_scenario = {}
            for n in self.Players:
                for e in self.Scenarios:
                    self.RevenuePlayers_per_scenario.update( 
						{(n,e) : (sum(self.powerHAUX2[n,e,t]*self.price[e,t] 
							for t in self.Time ) ) } )  
						
            # expected cost for community manager from buying
            self.CostCoMa_expected = ( 
				self.Weight*( sum( sum( -1*self.powerHAUX2[n,e,t]*self.price[e,t] 
					for n in self.Players ) 
					+ self.PriceBuy[t]*self.powerBuy[e,t] 
						for e in self.Scenarios for t in self.Time ) ) )  
						
			# scenario specific cost for community manager for buying
            self.CostCoMa_per_scenario = {}
            for e in self.Scenarios:
                self.CostCoMa_per_scenario.update( 
				    {e : sum( sum( -1*self.powerHAUX2[n,e,t]*self.price[e,t] 
						for n in self.Players ) 
						+ self.PriceBuy[t]*self.powerBuy[e,t] 
						for t in self.Time ) } )

		
		### cost--revenue from rights ###
		#################################
        if 'RightPayoffs' in Settings['eval']:
			# Costs
            self.RightsCost = {n : 0 for n in self.Players}
            for n in self.Players:
                self.RightsCost.update( 
                {n : sum( self.dual_ChRiBE[s]*self.chRiBid[n,s] + self.dual_DisRiBE[s]*self.disRiBid[n,s] + self.dual_SocRiBE[s]*self.socRiBid[n,s] 
					+ self.chRiBid[n,s]*self.chRiBid[n,s]*0.00000001
					+ self.disRiBid[n,s]*self.disRiBid[n,s]*0.00000001
					+ self.socRiBid[n,s]*self.socRiBid[n,s]*0.00000001 for s in self.Storage ) } )

           # Revenues
            self.RightsRev = {n : 0 for n in self.Players}
            for n in self.Players:
                self.RightsRev.update(
                {n : sum( self.dual_ChRiBE[s]*self.chRi[s] + self.dual_DisRiBE[s]*self.disRi[s] + self.dual_SocRiBE[s]*self.socRi[s] for s in self.StorageLoc[n] ) } )



        ### Results from dictonary to dataframe ###
		###########################################
        if 'spot' in Settings['how']:
		
			# operational results
            self.Operation = OrderedDict()
            for e in self.Scenarios:
			
                self.ResultsAUX = pd.DataFrame(
				  {('CoMa','powerBuy') :        [self.powerBuy.get((e,t)) for t in self.Time],
				   ('CoMa','powerSell') :       [self.powerSell.get((e,t)) for t in self.Time],
				   ('CoMa','dual_balanceLoc') : [self.dual_balanceLoc.get((e,t)) for t in self.Time]
				   }, index=self.Time )
				   
                for n in self.Players:
                    self.ResultsAUX[(n,'powerH')] =	[self.powerH.get((n,e,t)) for t in self.Time]
														
                for s in self.Storage:
                    self.ResultsAUX[(s,'powerCh')] =[self.powerCh.get((s,e,t)) for t in self.Time]
                    self.ResultsAUX[(s,'powerDis')] =[self.powerDis.get((s,e,t)) for t in self.Time]
                    self.ResultsAUX[(s,'soc')] =    [self.soc.get((s,e,t)) for t in self.Time]
					
                self.Operation.update({e : self.ResultsAUX})
		
		
            # Payoff expected    
            self.Payoff_expected = pd.DataFrame(
                    {'CoMa' : [0,0,0,0,0,0,0] }, index=['Cost','Rev','RightsCost','RightsRev','SurplusDist','RightsVal','Bal'] )        
            
            self.Payoff_expected.loc['Cost','CoMa'] = ( self.CostCoMa_expected )
            self.Payoff_expected.loc['Rev','CoMa'] = ( self.RevenueCoMa_expected )
            self.Payoff_expected.loc['Bal','CoMa'] = ( self.CostCoMa_expected + self.RevenueCoMa_expected )
    
            for n in self.Players:    
                self.Payoff_expected.loc['Cost',n] = ( self.CostPlayers_expected[n] )
                self.Payoff_expected.loc['Rev',n] = ( self.RevenuePlayers_expected[n] )
                self.Payoff_expected.loc['Bal',n] = ( self.CostPlayers_expected[n] + self.RevenuePlayers_expected[n] )
            self.Payoff_expected = self.Payoff_expected.fillna(0)
			
			# total expected payoff
            self.Payoff_expected.loc['Bal','Tot'] = ( self.Payoff_expected.sum(axis=1)['Bal'] )


            # Payoff per scenario
            self.Payoff_per_scenario = pd.DataFrame({}, index=self.Scenarios)       
            for n in self.Players:
                self.Payoff_per_scenario[n] = ( [self.CostPlayers_per_scenario[n,e] + self.RevenuePlayers_per_scenario[n,e] for e in self.Scenarios] )

			# total payoff per scenario
            self.Payoff_per_scenario['Tot'] = ( self.Payoff_per_scenario.sum(axis=1) )
					
					
					
        if 'spot_PSR' in Settings['how']:
		
			# operational results
            self.Operation = OrderedDict()
            for e in self.Scenarios:
			
                self.ResultsAUX = pd.DataFrame(
				  {('CoMa','powerBuy') :        [self.powerBuy.get((e,t)) for t in self.Time],
				   ('CoMa','powerSell') :       [self.powerSell.get((e,t)) for t in self.Time],
				   ('CoMa','dual_balanceLoc') : [self.dual_balanceLoc.get((e,t)) for t in self.Time]
				   }, index=self.Time )
				   
                for n in self.Players:
                    self.ResultsAUX[(n,'powerH')] =	[self.powerH.get((n,e,t)) for t in self.Time]
									
                for s in self.Storage:
                    for n in self.Players:
                        self.ResultsAUX[('%s_%s' % (n,s),'powerCh')] =[self.powerCh_PSR.get((n,s,e,t)) for t in self.Time]
                        self.ResultsAUX[('%s_%s' % (n,s),'powerDis')] =[self.powerDis_PSR.get((n,s,e,t)) for t in self.Time]
                        self.ResultsAUX[('%s_%s' % (n,s),'soc')] =    [self.soc_PSR.get((n,s,e,t)) for t in self.Time]
					
                self.Operation.update({e : self.ResultsAUX})
		
		
            # Payoff expected    
            self.Payoff_expected = pd.DataFrame(
                    {'CoMa' : [0,0,0,0,0,0,0] }, index=['Cost','Rev', 'RightsCost','RightsRev','SurplusDist','RightsVal', 'Bal'] )         
            
            self.Payoff_expected.loc['Cost','CoMa'] = ( self.CostCoMa_expected )
            self.Payoff_expected.loc['Rev','CoMa'] = ( self.RevenueCoMa_expected )
            self.Payoff_expected.loc['Bal','CoMa'] = ( self.CostCoMa_expected + self.RevenueCoMa_expected )
    
            for n in self.Players:    
                self.Payoff_expected.loc['Cost',n] = ( self.CostPlayers_expected[n] )
                self.Payoff_expected.loc['Rev',n] = ( self.RevenuePlayers_expected[n] )
                self.Payoff_expected.loc['RightsCost',n] = ( self.RightsCost[n] )
                self.Payoff_expected.loc['RightsRev',n] = ( self.RightsRev[n] )
                self.Payoff_expected.loc['Bal',n] = ( self.CostPlayers_expected[n] + self.RevenuePlayers_expected[n] + self.RightsCost[n] + self.RightsRev[n] )
            self.Payoff_expected = self.Payoff_expected.fillna(0)
 
			# total expected payoffs
            self.Payoff_expected.loc['Bal','Tot'] = (
				self.Payoff_expected.sum(axis=1)['Bal'] )
				
				
            # Payoff per scenario
            self.Payoff_per_scenario = pd.DataFrame({}, index=self.Scenarios)       
            for n in self.Players:
                self.Payoff_per_scenario[n] = ( [self.CostPlayers_per_scenario[n,e] + self.RevenuePlayers_per_scenario[n,e] + self.RightsCost[n] + self.RightsRev[n] for e in self.Scenarios] )					

			# total payoffs per scenario
            self.Payoff_per_scenario['Tot'] = ( self.Payoff_per_scenario.sum(axis=1) )    



        if 'spot_FSR' in Settings['how']:  
		
			# operational results
            self.Operation = OrderedDict()
            for e in self.Scenarios:
                self.ResultsAUX = pd.DataFrame(
				  {('CoMa','powerBuy') :        [self.powerBuy.get((e,t)) for t in self.Time],
				   ('CoMa','powerSell') :       [self.powerSell.get((e,t)) for t in self.Time],
				   ('CoMa','dual_balanceLoc') : [self.dual_balanceLoc_FSR.get((e,t)) for t in self.Time]
				   }, index=self.Time )
				   
                for n in self.Players:
                    self.ResultsAUX[(n,'powerH')] =	[self.powerH.get((n,e,t)) for t in self.Time]
					
                for s in self.Storage:
                    self.ResultsAUX[(s,'powerCh')] = [self.powerCh_FSR.get((s,e,t)) for t in self.Time]
                    self.ResultsAUX[(s,'powerDis')] = [self.powerDis_FSR.get((s,e,t)) for t in self.Time]
                    self.ResultsAUX[(s,'soc')] =    [self.soc_FSR.get((s,e,t)) for t in self.Time]
					
                self.Operation.update({e : self.ResultsAUX})
                  
				  
            # Payoff expected    
            self.Payoff_expected = pd.DataFrame(
                    {'CoMa' : [0,0,0,0,0,0,0] }, index=['Cost','Rev', 'RightsCost','RightsRev','SurplusDist','RightsVal', 'Bal'] )        
            
            self.Payoff_expected.loc['Cost','CoMa'] = ( self.CostCoMa_expected )
            self.Payoff_expected.loc['Rev','CoMa'] = ( self.RevenueCoMa_expected )

            for n in self.Players:    
                self.Payoff_expected.loc['Cost',n] = ( self.CostPlayers_expected[n] )
                self.Payoff_expected.loc['Rev',n] = ( self.RevenuePlayers_expected[n] )
            self.Payoff_expected = self.Payoff_expected.fillna(0)


            # Payoff per scenario
            self.Payoff_per_scenario = pd.DataFrame({}, index=self.Scenarios)       
            for n in self.Players:
                self.Payoff_per_scenario[n] = ( [self.CostPlayers_per_scenario[n,e] + self.RevenuePlayers_per_scenario[n,e] for e in self.Scenarios] )				        
                

        if 'frwd_FSR' in Settings['how']:  
		
            # payoffs in FSR forward market
            self.payoff = pd.DataFrame(
                    {'CoMa' : [0,0,0,0,0,0,0] }, index=['Cost','Rev', 'RightsCost','RightsRev','SurplusDist','RightsVal', 'Bal'] )        
               
            for n in self.Players:  
                self.payoff.loc['RightsCost',n] = self.RightsCost[n]   
                self.payoff.loc['RightsRev',n] = self.RightsRev[n]            
            self.payoff = self.payoff.fillna(0)				