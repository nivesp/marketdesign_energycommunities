''' 
=========================================================================
Script         : Generates and solves opt problems
Author         : Niklas Vepermann
Project        : Energy communities and local markets
=========================================================================
''' 
import pandas as pd    # Pandas
import numpy as np
import time
from collections import OrderedDict 

import matplotlib.pyplot as plt

from OptProblem import OptProblem
from func.PVScenarios import gen_PVInput
from func.PVScenarios import clustered_PV


# load model settings
exec(open('./data/input/ModelSettings.py').read())

# generate PV power scenarios
PV_mean = [0.0, 0.0, 0.0, 0.0, 0.01, 0.1, 0.2, 0.3, 0.5, 0.6, 0.8, 0.85, 0.8, 0.8, 0.7, 0.6, 0.5, 0.3, 0.2, 0.1, 0.01, 0.0, 0.0, 0.0] # Expected PV power generation	
			
Number_Samples = 1000 # Sampling scenarios

spot_var_lb = pd.DataFrame()
spot_var_ub = pd.DataFrame()
spot_payoff = pd.DataFrame()

spot_PSR_var_lb = pd.DataFrame()
spot_PSR_var_ub = pd.DataFrame()
spot_PSR_payoff = pd.DataFrame()

spot_FSR_var_lb = pd.DataFrame()
spot_FSR_var_ub = pd.DataFrame()
spot_FSR_payoff = pd.DataFrame()

Coop_Shap_var_lb = pd.DataFrame()
Coop_Shap_var_ub = pd.DataFrame()
Coop_Shap_payoff = pd.DataFrame()

Coop_Nucl_var_lb = pd.DataFrame()
Coop_Nucl_var_ub = pd.DataFrame()
Coop_Nucl_payoff = pd.DataFrame()

time_spot = []
time_PSR = []
time_FSR = []
time_Coop = []
time_Shap = []
time_Nucl = []

#Range_of_study = np.arange(0.01, 0.11, 0.01)
Range_of_study = [0.025]

############################
CalcPayoffPerScenario = False
PlotIncVar = False
############################


start = time.time()

# set number of players
startPlayer = 11
stopPlayer = 11

for NumbPlayers in list(range(startPlayer,stopPlayer+1)):

	for var in Range_of_study:

		from func.PVScenarios import gen_PVScenarios
		FullSample_of_PV_AUX = gen_PVScenarios(PV_mean,Number_Samples,var) # generate scenarios

		training = 2
		FullSample_of_PV = clustered_PV(FullSample_of_PV_AUX,training)

			
		# load data
		exec(open('./data/input/InputData.py').read())


		########
		# SPOT #
		########
		start_spot = time.time()

		model_name = 'spot'
		PV_scenarios = FullSample_of_PV[0:training]
		Weight,Scenarios,PowerPV = gen_PVInput(training,PV_scenarios,Players,Time)

		spot_Input = 	{'Players' : Players,
						 'Scenarios' : Scenarios,
						 'Time' : Time,
						 'PriceBuy' : PriceBuy,
						 'PriceSell' : PriceSell,
						 'Weight' : Weight,
						 'PowerPV' : PowerPV,
						 'Demand' : Demand,
						 'Storage' : Storage,
						 'StorageLoc' : StorageLoc,
						 'EffCh' : EffCh,
						 'EffDis' : EffDis,
						 'SocIni' : SocIni,
						 'ChMax' : ChMax,
						 'DisMax' : DisMax,
						 'SocMax' : SocMax}  

		# Initialize model and run Optimization							
		spot = OptProblem(model_name,spot_Input,spot_Settings) 
		spot.run_opt()
		spot.get_results(spot_Settings)

		# Collect results
		#################
		spot_var_lb = spot_var_lb.append(pd.DataFrame( spot.Payoff_per_scenario.mean() - spot.Payoff_per_scenario.std()).transpose(), ignore_index=True)
		spot_var_ub = spot_var_ub.append(pd.DataFrame( spot.Payoff_per_scenario.mean() + spot.Payoff_per_scenario.std()).transpose(), ignore_index=True)
		spot_payoff = spot_payoff.append( spot.Payoff_per_scenario.mean(), ignore_index=True)

		end_spot = time.time()
		time_spot.append(round(end_spot-start_spot,2))


		##############
		# SPOT + PSR #
		##############	
		start_PSR = time.time()

		model_name = 'spot_PSR'
		PV_scenarios = FullSample_of_PV[0:training]
		Weight,Scenarios,PowerPV = gen_PVInput(training,PV_scenarios,Players,Time)

		spot_PSR_Input = {'Players' : Players,
						 'Scenarios' : Scenarios,
						 'Time' : Time,
						 'PriceBuy' : PriceBuy,
						 'PriceSell' : PriceSell,
						 'Weight' : Weight,
						 'PowerPV' : PowerPV,
						 'Demand' : Demand,
						 'Storage' : Storage,
						 'StorageLoc' : StorageLoc,
						 'EffCh' : EffCh,
						 'EffDis' : EffDis,
						 'SocIni' : SocIni,
						 'ChMax' : ChMax,
						 'DisMax' : DisMax,
						 'SocMax' : SocMax}     

		# Initialize model and run Optimization							
		spot_PSR = OptProblem(model_name,spot_PSR_Input,spot_PSR_Settings) 
		spot_PSR.run_opt()
		spot_PSR.get_results(spot_PSR_Settings)	

		# Collect results
		#################
		spot_PSR_var_lb = spot_PSR_var_lb.append(pd.DataFrame( spot_PSR.Payoff_per_scenario.mean() - spot_PSR.Payoff_per_scenario.std()).transpose(), ignore_index=True)
		spot_PSR_var_ub = spot_PSR_var_ub.append(pd.DataFrame( spot_PSR.Payoff_per_scenario.mean() + spot_PSR.Payoff_per_scenario.std()).transpose(), ignore_index=True)
		spot_PSR_payoff = spot_PSR_payoff.append( spot_PSR.Payoff_per_scenario.mean(), ignore_index=True)

		end_PSR = time.time()
		time_PSR.append(round(end_PSR-start_PSR,2))


		##############
		# SPOT + FSR #
		##############
		start_FSR = time.time()
		
		model_name = 'spot_FSR'
		
		PV_scenarios = FullSample_of_PV[0:training]
		Weight,Scenarios,PowerPV = gen_PVInput(training,PV_scenarios,Players,Time)

		spot_FSR_Input = {'Players' : Players,
						 'Scenarios' : Scenarios,
						 'Time' : Time,
						 'PriceBuy' : PriceBuy,
						 'PriceSell' : PriceSell,
						 'Weight' : Weight,
						 'PowerPV' : PowerPV,
						 'Demand' : Demand,
						 'Storage' : Storage,
						 'StorageLoc' : StorageLoc,
						 'EffCh' : EffCh,
						 'EffDis' : EffDis,
						 'SocIni' : SocIni,
						 'ChMax' : ChMax,
						 'DisMax' : DisMax,
						 'SocMax' : SocMax}   

		# Initialize model and run Optimization
		spot_FSR = OptProblem(model_name,spot_FSR_Input,spot_FSR_Settings)
		spot_FSR.run_opt()
		spot_FSR.get_results(spot_FSR_Settings)	


		# get duals from multi period optimization 
		spot_FSR.dual_ChMax = OrderedDict({key: (1/Weight)*abs(value) for key,value in spot_FSR.dual_ChMax_FSR.items()})
		spot_FSR.dual_DisMax = OrderedDict({key: (1/Weight)*abs(value) for key,value in spot_FSR.dual_DisMax_FSR.items()})
		spot_FSR.dual_SocMax = OrderedDict({key: (1/Weight)*abs(value) for key,value in spot_FSR.dual_SocMax_FSR.items()})
			
		# Initialize players valuation of FSR
		ValueChRi = spot_FSR.dual_ChMax
		ValueDisRi = spot_FSR.dual_DisMax
		ValueSocRi = spot_FSR.dual_SocMax

		# FSR auction
		model_name = 'frwd_FSR'

		frwd_FSR_Input = {'Players' : Players,
						 'Scenarios' : Scenarios,
						 'Time' : Time,
						 'Weight' : Weight,
						 'Storage' : Storage,
						 'StorageLoc' : StorageLoc,
						 'ChMax' : ChMax,
						 'DisMax' : DisMax,
						 'SocMax' : SocMax,
						 'ValueChRi' : ValueChRi,
						 'ValueDisRi' : ValueDisRi,
						 'ValueSocRi' : ValueSocRi} 					 

		# Initialize model and run Optimization							
		frwd_FSR = OptProblem(model_name,frwd_FSR_Input,frwd_FSR_Settings) 
		frwd_FSR.run_opt()
		frwd_FSR.get_results(frwd_FSR_Settings)	


		# adding FSR forward market payoffs to expected payoffs
		spot_FSR.Payoff_expected = spot_FSR.Payoff_expected + frwd_FSR.payoff

		# adding FSR value to expected payoffs
		for n in spot_FSR.Players: 
			spot_FSR.Payoff_expected.loc[spot_FSR.Payoff_expected.index[5],n] = (
			#
			-1*spot_FSR.Weight*sum( spot_FSR.dual_ChMax[s,e,t]*frwd_FSR.chRiBid[n,s] + spot_FSR.dual_DisMax[s,e,t]*frwd_FSR.disRiBid[n,s] + spot_FSR.dual_SocMax[s,e,t]*frwd_FSR.socRiBid[n,s] 
			#
			for s in spot_FSR.Storage for e in spot_FSR.Scenarios for t in spot_FSR.Time ) )

		# adding surplus distribution to expected payoffs
		spot_FSR.Payoff_expected.loc['SurplusDist','CoMa'] = ( 
			#
			spot_FSR.Weight*sum( spot_FSR.dual_ChMax[s,e,t]*frwd_FSR.chRiBid[n,s] + spot_FSR.dual_DisMax[s,e,t]*frwd_FSR.disRiBid[n,s] + spot_FSR.dual_SocMax[s,e,t]*frwd_FSR.socRiBid[n,s]
			#
			for s in spot_FSR.Storage for n in spot_FSR.Players for e in spot_FSR.Scenarios for t in spot_FSR.Time ) )

		# adding balance and total expected payoffs
		spot_FSR.Payoff_expected.loc['Bal'] = spot_FSR.Payoff_expected.sum(axis=0)	
		spot_FSR.Payoff_expected.loc['Bal','Tot'] = ( spot_FSR.Payoff_expected.sum(axis=1)['Bal'] )


		# adding FSR forward market payoffs and FSR values to payoffs per scenario
		for n in spot_FSR.Players:
			spot_FSR.Payoff_per_scenario[n] = (
			#
			[spot_FSR.Payoff_per_scenario[n][e] + frwd_FSR.payoff[n]['RightsCost'] + frwd_FSR.payoff[n]['RightsRev']
			-1*sum(spot_FSR.dual_ChMax[s,e,t]*frwd_FSR.chRiBid[n,s] + spot_FSR.dual_DisMax[s,e,t]*frwd_FSR.disRiBid[n,s] + spot_FSR.dual_SocMax[s,e,t]*frwd_FSR.socRiBid[n,s] for s in spot_FSR.Storage for t in spot_FSR.Time)
			#
			for e in spot_FSR.Scenarios] )
			
		# adding total payoffs to payoffs per scenario
		spot_FSR.Payoff_per_scenario['Tot'] = ( spot_FSR.Payoff_per_scenario.sum(axis=1) )
			
			
		# Collect results
		#################
		spot_FSR_var_lb = spot_FSR_var_lb.append(pd.DataFrame( spot_FSR.Payoff_per_scenario.mean() - spot_FSR.Payoff_per_scenario.std()).transpose(), ignore_index=True)
		spot_FSR_var_ub = spot_FSR_var_ub.append(pd.DataFrame( spot_FSR.Payoff_per_scenario.mean() + spot_FSR.Payoff_per_scenario.std()).transpose(), ignore_index=True)
		spot_FSR_payoff = spot_FSR_payoff.append(spot_FSR.Payoff_per_scenario.mean(), ignore_index=True)

		end_FSR = time.time()
		time_FSR.append(round(end_FSR-start_FSR,2))		
		

		###############
		# Cooperative #
		###############
		start_Coop = time.time()
		
		model_name = 'Coop'		
		
		PV_scenarios = FullSample_of_PV[0:training]
		Weight,Scenarios,PowerPV = gen_PVInput(training,PV_scenarios,Players,Time)

		
		from func.Cooperative_func import power_set, extract, atoi, natural_keys
		all_coalitions = power_set(Players)

		
		# calculate cooperative game in parallel
		from joblib import Parallel, delayed
		import multiprocessing
		
		num_cores = multiprocessing.cpu_count()

		Coop_Input = {'Players' : 	Players,
					 'Scenarios' : 	Scenarios,
					 'Time' : 		Time,
					 'PriceBuy' : 	PriceBuy,
					 'PriceSell' : 	PriceSell,
					 'Weight' : 	Weight,
					 'PowerPV' : 	PowerPV,
					 'Demand' : 	Demand,
					 'EffCh' : 		EffCh,
					 'EffDis' : 	EffDis,
					 'SocIni' : 	SocIni,
					 'Storage' : 	Storage,
					 'StorageLoc' : StorageLoc,
					 'ChMax' : 		ChMax,
					 'DisMax' : 	DisMax,
					 'SocMax' : 	SocMax} 
					 
		Coop = OptProblem(model_name,Coop_Input,Coop_Settings)  
		
		from func.Cooperative_func import calc_Coop
				
		Parallel_Results = Parallel(n_jobs=num_cores)(delayed(calc_Coop)(coalition,Coop_Input,model_name,Coop_Settings) for coalition in all_coalitions)
						
		cost_all_coalitions_expected = OrderedDict()
		cost_of_all_coalitions_per_scenario = OrderedDict()
		
		for c in range(0,len(all_coalitions)):
			cost_all_coalitions_expected.update(Parallel_Results[c][0])
			cost_of_all_coalitions_per_scenario.update(Parallel_Results[c][1])

		
		# for coalition in all_coalitions:
			
			# Coop_Input = 	{'Players' : 	coalition,
							 # 'Scenarios' : 	Scenarios,
							 # 'Time' : 		Time,
							 # 'PriceBuy' : 	PriceBuy,
							 # 'PriceSell' : 	PriceSell,
							 # 'Weight' : 	Weight,
							 # 'PowerPV' : 	extract(PowerPV, [(n,e,t) 
												# for n in coalition 
												# for e in Scenarios 
												# for t in Time]),
							 # 'Demand' : 	extract(Demand, [(n,t) 
												# for n in coalition
												# for t in Time]),
							 # 'EffCh' : 		EffCh,
							 # 'EffDis' : 	EffDis,
							 # 'SocIni' : 	SocIni,
							 # 'Storage' : 	Storage,
							 # 'StorageLoc' : StorageLoc,
							 # 'ChMax' : 		ChMax,
							 # 'DisMax' : 	DisMax,
							 # 'SocMax' : 	SocMax}  

			# # Initialize model and run Optimization							
			# Coop = OptProblem(model_name,Coop_Input,Coop_Settings) 
			# Coop.run_opt()
			# Coop.get_results(Coop_Settings)
			
			# # expected
			# cost_of_coalition_expected = {tuple(coalition) : Coop.m.objVal}
			# cost_all_coalitions_expected.update(cost_of_coalition_expected)
			
			# # per scenario
			# cost_of_coalition_per_scenario = {tuple(coalition) : OrderedDict({e : sum( Coop.PriceBuy[t]*Coop.powerBuy[e,t] - Coop.PriceSell[t]*Coop.powerSell[e,t] for t in Coop.Time ) for e in Scenarios}) }	
			# cost_of_all_coalitions_per_scenario.update(cost_of_coalition_per_scenario)

		# expected
		##########
		# calculate expected value of coalition as expected cost saving
		Coop.value_expected = OrderedDict()
		for coalition in all_coalitions:
			value_AUX = {tuple(coalition) : sum(cost_all_coalitions_expected[tuple(coalition)[n],] for n in range(0,len(coalition))) - cost_all_coalitions_expected[tuple(coalition)]}
			Coop.value_expected.update(value_AUX)

		# per scenario
		##############
		# calculate value per scenario of coalition as cost saving per scenario
		Coop.value_per_scenario = OrderedDict()
		for coalition in all_coalitions:
			value_AUX = {tuple(coalition) : OrderedDict({e : sum(cost_of_all_coalitions_per_scenario[tuple(coalition)[n],][e] for n in range(0,len(coalition))) - cost_of_all_coalitions_per_scenario[tuple(coalition)][e] for e in Scenarios}) } 
			Coop.value_per_scenario.update(value_AUX)

		end_Coop = time.time()
		time_Coop.append(round(end_Coop-start_Coop,2))
		

		###############
		# Coop - Shap #
		###############
		start_Shap = time.time()
	
		from func.Cooperative_func import shapley

		model_name = 'Coop_Shap'
		Coop_Shap = OptProblem(model_name,Coop_Input,Coop_Settings)

		# expected
		##########
		# calculate expected shapely value 		
		Coop_Shap.list_of_values = list(Coop.value_expected.values())
		Coop_Shap.shapley_value_expected = shapley(Players,all_coalitions, Coop_Shap.list_of_values)	

		# calcualte expected individual costs in grand coalition
		Coop_Shap.Payoff_expected = OrderedDict()
		for n in Players:
			Payoff_expected_AUX = {n : cost_all_coalitions_expected[n,] - Coop_Shap.shapley_value_expected[n]}
			Coop_Shap.Payoff_expected.update(Payoff_expected_AUX)


		################################
		if CalcPayoffPerScenario == True:
		################################
		
			# per scenario
			##############
			# calculate shapely value per scenario
			Coop_Shap.shapley_value_per_scenario = OrderedDict()
			for e in Scenarios:
				Coop_Shap.list_of_values = list(Coop.value_per_scenario[tuple(coalition)][e] for coalition in all_coalitions)
				Coop_Shap.shapley_value_AUX = OrderedDict({e : shapley(Players,all_coalitions, Coop_Shap.list_of_values)})
				Coop_Shap.shapley_value_per_scenario.update(Coop_Shap.shapley_value_AUX)

			# calcualte individual costs per scenario in grand coalition
			Coop_Shap.Payoff_per_scenario_AUX0 = OrderedDict()
			for e in Scenarios:
				Payoff_per_scenario_AUX1 = {e : OrderedDict({n : cost_of_all_coalitions_per_scenario[n,][e] - Coop_Shap.shapley_value_per_scenario[e][n] for n in Players}) }
				Coop_Shap.Payoff_per_scenario_AUX0.update(Payoff_per_scenario_AUX1)

			# dict to DataFrame
			Coop_Shap.Payoff_per_scenario = pd.DataFrame.from_dict( Coop_Shap.Payoff_per_scenario_AUX0, orient='index')
			Coop_Shap.Payoff_per_scenario['Tot'] = ( Coop_Shap.Payoff_per_scenario.sum(axis=1) )

			# Collect results
			#################
			Coop_Shap_var_lb = Coop_Shap_var_lb.append(pd.DataFrame( Coop_Shap.Payoff_per_scenario.mean() - Coop_Shap.Payoff_per_scenario.std()).transpose(), ignore_index=True)
			Coop_Shap_var_ub = Coop_Shap_var_ub.append(pd.DataFrame( Coop_Shap.Payoff_per_scenario.mean() + Coop_Shap.Payoff_per_scenario.std()).transpose(), ignore_index=True)
			Coop_Shap_payoff = Coop_Shap_payoff.append(Coop_Shap.Payoff_per_scenario.mean(), ignore_index=True)

		end_Shap = time.time()	
		time_Shap.append(round(end_Shap-start_Shap,2))			
			
			
		###############
		# Coop - Nucl #
		###############
		start_Nucl= time.time()
	
		from func.Cooperative_func import nucleolus

		model_name = 'Coop_Nucl'
		Coop_Nucl = OptProblem(model_name,Coop_Input,Coop_Settings)

		# expected
		##########
		# calculate expected nucleolus
		Coop_Nucl.nucl_expected = nucleolus(Players,all_coalitions,Coop.value_expected)

		# calcualte expected individual costs in grand coalition
		Coop_Nucl.Payoff_expected = OrderedDict()
		for n in Players:
			Payoff_expected_AUX = {n : cost_all_coalitions_expected[n,] - Coop_Nucl.nucl_expected[n]}
			Coop_Nucl.Payoff_expected.update(Payoff_expected_AUX)

		
		################################
		if CalcPayoffPerScenario == True:
		################################
		
			# per scenario
			##############	
			# calculate nucleolus per scenario in parallel
			from func.Cooperative_func import calc_Nucl
			
			num_cores = multiprocessing.cpu_count()
			
			value_per_scenario_AUX = Coop.value_per_scenario
			Parallel_Results = Parallel(n_jobs= num_cores, prefer="threads")(delayed(calc_Nucl)(s,Players,all_coalitions,value_per_scenario_AUX) for s in Scenarios)
			#result = calc_Nucl('s1',Players,all_coalitions,Coop.value_per_scenario)
			
			Coop_Nucl.nucl_per_scenario = OrderedDict()
			
			for s in range(0,len(Scenarios)):
				Coop_Nucl.nucl_per_scenario.update(Parallel_Results[s])
				
				
			# # calculate nucleolus per scenario
			# Coop_Nucl.nucl_per_scenario = OrderedDict()
			# for s in Scenarios:
				# print('\n Scenario is: %s \n' % s )
				# Coop_Nucl.dict_of_values = { tuple(coalition) : Coop.value_per_scenario[tuple(coalition)][s] for coalition in all_coalitions }
				# Coop_Nucl.nucleolus_AUX = OrderedDict({s : nucleolus(Players,all_coalitions, Coop_Nucl.dict_of_values)})
				# Coop_Nucl.nucl_per_scenario.update(Coop_Nucl.nucleolus_AUX)


			# calcualte individual costs per scenario in grand coalition
			Coop_Nucl.Payoff_per_scenario_AUX0 = OrderedDict()
			for s in Scenarios:
				Payoff_per_scenario_AUX1 = {s : OrderedDict({n : cost_of_all_coalitions_per_scenario[n,][s] - Coop_Nucl.nucl_per_scenario[s][n] for n in Players}) }
				Coop_Nucl.Payoff_per_scenario_AUX0.update(Payoff_per_scenario_AUX1)

			# dict to DataFrame
			Coop_Nucl.Payoff_per_scenario = pd.DataFrame.from_dict( Coop_Nucl.Payoff_per_scenario_AUX0, orient='index')
			Coop_Nucl.Payoff_per_scenario['Tot'] = ( Coop_Nucl.Payoff_per_scenario.sum(axis=1) )

			# Collect results
			#################
			Coop_Nucl_var_lb = Coop_Nucl_var_lb.append(pd.DataFrame( Coop_Nucl.Payoff_per_scenario.mean() - Coop_Nucl.Payoff_per_scenario.std()).transpose(), ignore_index=True)
			Coop_Nucl_var_ub = Coop_Nucl_var_ub.append(pd.DataFrame( Coop_Nucl.Payoff_per_scenario.mean() + Coop_Nucl.Payoff_per_scenario.std()).transpose(), ignore_index=True)
			Coop_Nucl_payoff = Coop_Nucl_payoff.append(Coop_Nucl.Payoff_per_scenario.mean(), ignore_index=True)

		end_Nucl = time.time()			
		time_Nucl.append(round(end_Nucl-start_Nucl,2))		


		#########################
		# plot result in-sample #
		#########################

		# plot PV production
		from func.Eval_plot import plot_PV
		PV_scenarios = FullSample_of_PV[0:10]
		plot_PV(PV_mean,PV_scenarios)


		# plot wholesale prices
		from func.Eval_plot import plot_WholesalePrices	
		plot_WholesalePrices(PriceBuy,PriceSell)


		# plot local prices
		from func.Eval_plot import plot_LocalPrices
		LocalPrice = []
		for s in Scenarios:
			LocalPrice.append(list(spot.Operation[s]['CoMa']['dual_balanceLoc']))
		LocalPrice_expected = sum(spot.Operation[s]['CoMa']['dual_balanceLoc'] 
			for s in Scenarios) / len(Scenarios)
		plot_LocalPrices(PriceBuy,PriceSell,LocalPrice,LocalPrice_expected)


		# plot in-sample payflows
		from func.Eval_plot import bar_plot
		EvalSet = [spot.Payoff_expected,spot_PSR.Payoff_expected,spot_FSR.Payoff_expected]
		modelname = ['spot','spot+PSR','spot+FSR']

		z = 0
		for x in EvalSet:
			InputDF = x
			bar_plot(InputDF,modelname[z])
			z = z + 1

		################################
		if CalcPayoffPerScenario == True:
		################################
		
			# plot in-sample total cost distribution
			totalCost_distribution = pd.DataFrame({'spot' : 
					np.zeros(len(spot.Payoff_per_scenario)).tolist()},
					index=spot.Payoff_per_scenario.index.tolist())
					
			totalCost_distribution['spot'] = spot.Payoff_per_scenario['Tot']
			totalCost_distribution['spot_PSR'] = spot_PSR.Payoff_per_scenario['Tot']
			totalCost_distribution['spot_FSR'] = spot_FSR.Payoff_per_scenario['Tot']
			totalCost_distribution['Coop_Shap'] = Coop_Shap.Payoff_per_scenario['Tot']
			totalCost_distribution['Coop_Nucl'] = Coop_Nucl.Payoff_per_scenario['Tot']
			
			LBAux = totalCost_distribution[totalCost_distribution.lt(totalCost_distribution.quantile(0.05,axis=0, interpolation='higher'), axis=1)].mean()
			UBAux = totalCost_distribution[totalCost_distribution.gt(totalCost_distribution.quantile(0.95,axis=0, interpolation='higher'), axis=1)].mean()
					
			from func.Eval_plot import totalCost_dist
			totalCost_dist(totalCost_distribution,LBAux,UBAux)


			# plot individual payoff distribution
			payoff_distribution = pd.DataFrame({('Player1','spot') : 
				np.zeros(len(spot_FSR.Payoff_per_scenario)).tolist()},
				index=spot_FSR.Payoff_per_scenario.index.tolist())

			for n in Players:
				payoff_distribution[(n,'spot')] = spot.Payoff_per_scenario[n]
				payoff_distribution[n,'spot_PSR'] = spot_PSR.Payoff_per_scenario[n]
				payoff_distribution[n,'spot_FSR'] = spot_FSR.Payoff_per_scenario[n]
				payoff_distribution[n,'Coop_Shap'] = Coop_Shap.Payoff_per_scenario[n]
				payoff_distribution[n,'Coop_Nucl'] = Coop_Nucl.Payoff_per_scenario[n]

			LBAux = payoff_distribution[payoff_distribution.lt(payoff_distribution.quantile(0.05,axis=0, interpolation='higher'), axis=1)].mean()
			UBAux = payoff_distribution[payoff_distribution.gt(payoff_distribution.quantile(0.95,axis=0, interpolation='higher'), axis=1)].mean()
			
			from func.Eval_plot import payoff_dist
			payoff_dist(payoff_distribution,Players,LBAux,UBAux)


		print('\n Variance is: %s \n' % var )
	
	print('\n Number of Player is: %s \n' % NumbPlayers )
	
end = time.time()
print('\n Solving Time: %d min \n' % (round((end - start)/60,2)) )	


################################
if CalcPayoffPerScenario == True:
	if PlotIncVar == True:
################################

		#################################
		# Impact of increasing variance #
		#################################

		from func.Eval_plot import plot_inc_var

		case = ['spot','spot_PSR','spot_FSR','Coop_Shap','Coop_Nucl']
		payoff = [spot_payoff,spot_PSR_payoff,spot_FSR_payoff,Coop_Shap_payoff,Coop_Nucl_payoff]
		var_lb = [spot_var_lb,spot_PSR_var_lb,spot_FSR_var_lb,Coop_Shap_var_lb,Coop_Nucl_var_lb]
		var_ub = [spot_var_ub,spot_PSR_var_ub,spot_FSR_var_ub,Coop_Shap_var_ub,Coop_Nucl_var_ub]

		# Activate to plot impact of increasing risk aversion:
		for x in range(0,len(case)):
			plot_inc_var(case[x],Range_of_study,payoff[x],var_lb[x],var_ub[x])


######################
# Computational time #
######################
	
from func.Eval_plot import plot_time
plot_time(Players,startPlayer,stopPlayer,time_spot,time_PSR,time_FSR,time_Coop,time_Shap,time_Nucl)	


# Quantiles
###########
# payoff_distribution.quantile(0.05,axis=0, interpolation='higher')
# payoff_distribution[payoff_distribution.lt(payoff_distribution.quantile(0.90,axis=0, interpolation='higher'), axis=1)].mean()