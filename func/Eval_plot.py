import matplotlib.pyplot as plt
import tikzplotlib
import numpy as np
from datetime import datetime

def bar_plot(InputDF,modelname):   
	N = len(InputDF.columns)
	ind = np.arange(N)    # the x locations for the groups
	width = 0.2          # the width of the bars: can also be len(x) sequence
	sep = 0.025

	fig1 = plt.figure()
	ax = fig1.add_subplot(111)
	
	# Forward
	p3 = ax.bar(ind - width - sep, InputDF.iloc[2].tolist(), width, 
		color='orange', label='right cost') # Rights Costs
	p4 = ax.bar(ind - width - sep, InputDF.iloc[3].tolist(), width, color='olive',
		label='right revenue') # Rights Revenue
		
	# Spot
	p1 = ax.bar(ind, InputDF.iloc[0].tolist(), width, color='red',
		label='operating cost') # Operation Costs
	p4 = ax.bar(ind, InputDF.iloc[4].tolist(), width, 
		bottom=np.add(InputDF.iloc[2].tolist(),
		InputDF.iloc[0].tolist()).tolist(), color='purple',
		label='surplus dist') # congestion surplus distribution
	p2 = ax.bar(ind, InputDF.iloc[1].tolist(), width, color='g',
		label='operating revenue') # Operation Revenue
	p5 = ax.bar(ind, InputDF.iloc[5].tolist(), width, 
		bottom=InputDF.iloc[1].tolist(), color='darkcyan',
		label='right value') # Rights Values
		
	# Balance
	p6 = ax.bar(ind + width + sep, InputDF.iloc[6].tolist(), width, color='grey', 
		label='balance') # Balance

	# Add value of the balance
	for i, v in enumerate(InputDF.iloc[6].tolist()):
		if v >= 0:
			ax.text(i + width, 1.05*v, str(round(v,2)), color='grey', 
				ha='center', va='bottom')
		if v < 0:
			ax.text(i + width, 1.05*v, str(round(v,2)), color='grey', 
				ha='center', va='top')
	
	# Add some text for labels, title and custom x-axis tick labels, etc.
	#ax.set_ylim(min(InputDF.iloc[3].tolist()),max(InputDF.iloc[0].tolist()))
	ax.set_ylabel('Payoffs [EUR]')
	ax.set_xticks(ind)
	ax.set_xticklabels(list(InputDF.columns.values))
	ax.axhline(y=0, color='black', linewidth=0.5)
	ax.legend(ncol=3,loc='lower center', bbox_to_anchor=(0.5, 0.0),
		prop={'size': 6})
	plt.title(str('Payflows' + '_' + modelname))  
	tikzplotlib.save('./data/output/graphics/%s_Payflows_%s.tex' 
		% (datetime.today().strftime('%Y%m%d'), modelname), 
			   axis_height = '\\fheight',
			   axis_width = '\\fwidth')
	plt.savefig('./data/output/graphics/%s_Payflows_%s.pdf' 
		% (datetime.today().strftime('%Y%m%d'), modelname), 
		format='pdf', dpi=1000) 
	return


def totalCost_dist(totalCost_distribution,LB,UB):
	fig1 = plt.figure()
	medianprops = dict(linestyle=None, linewidth=0, color='firebrick')
	ax = fig1.add_subplot(111)    
	ax.boxplot(totalCost_distribution.transpose(), whis=[5, 95], showfliers=False, showmeans=True, medianprops=medianprops)
	plt.scatter([1, 2, 3, 4, 5],LB, marker='o', color='r')
	plt.scatter([1, 2, 3, 4, 5],UB, marker='o', color='r')
	plt.title('Total Cost')
	ax.set_ylabel('Payoffs [EUR]')
	plt.xticks([1, 2, 3, 4, 5], ['Spot', 'Spot+PSR', 'Spot+FSR', 'Coop+Shap', 'Coop+Nucl'])
	tikzplotlib.save('./data/output/graphics/%s_TotalCostDist.tex' 
		% (datetime.today().strftime('%Y%m%d')),
			   axis_height = '\\fheight',
			   axis_width = '\\fwidth')
	plt.savefig('./data/output/graphics/%s_TotalCostDist.pdf' 
		% (datetime.today().strftime('%Y%m%d')),
		format='pdf', dpi=1000)
	return
	
	
	
def payoff_dist(payoff_distribution,Players,LB,UB):
	for n in Players:
		fig1 = plt.figure()
		medianprops = dict(linestyle=None, linewidth=0, color='firebrick')
		ax = fig1.add_subplot(111)    
		ax.boxplot(payoff_distribution[n].transpose(), whis=[5, 95], 
			showfliers=False, showmeans=True, medianprops=medianprops)
		plt.scatter([1, 2, 3, 4, 5],LB[n], marker='o', color='r')
		plt.scatter([1, 2, 3, 4, 5],UB[n], marker='o', color='r')
		plt.xticks([1, 2, 3, 4, 5], ['Spot', 'Spot+PSR', 'Spot+FSR', 'Coop+Shap', 'Coop+Nucl'])
		ax.set_ylabel('Payoffs [EUR]')
		plt.title(n)
		tikzplotlib.save('./data/output/graphics/%s_PayoffDist_%s.tex' 
			% (datetime.today().strftime('%Y%m%d'), n), 
			   axis_height = '\\fheight',
			   axis_width = '\\fwidth')
		plt.savefig('./data/output/graphics/%s_PayoffDist_%s.pdf' 
			% (datetime.today().strftime('%Y%m%d'), n), 
			format='pdf', dpi=1000)
	return	


def plot_PV(PV_mean,PV_scenarios):
	t = [x for x in range(1,len(PV_mean)+1)]
	z1 = PV_scenarios
	
	fig1 = plt.figure()
	ax1 = fig1.add_subplot(111)
	ax1.plot(t,np.transpose(z1),color='k')
	ax1.plot(t,np.transpose(z1[0]),color='k',label='Scenarios')
	ax1.plot(t,PV_mean,color='r',label='Expectation')
	ax1.set_ylabel('PV generation [-]')
	ax1.set_xlabel('Time')
	ax1.set_xlim(1,24)
	ax1.legend()
	tikzplotlib.save('./data/output/graphics/%s_PV_generation.tex' 
		% (datetime.today().strftime('%Y%m%d')),
			   axis_height = '\\fheight',
			   axis_width = '\\fwidth')
	plt.savefig('./data/output/graphics/%s_PV_generation.pdf' 
		% (datetime.today().strftime('%Y%m%d')),
		format='pdf', dpi=1000)
		

def plot_WholesalePrices(PriceBuy,PriceSell):
	t = [x for x in range(1,len(list(PriceBuy.values()))+1)]
    
	fig1 = plt.figure()
	ax1 = fig1.add_subplot(111)
	plt.step(t,list(PriceBuy.values()),where='post',linestyle='dashed',color='red',label='Import price')
	plt.step(t,list(PriceSell.values()),where='post',linestyle='dotted',color='green',label='Export price')    
	ax1.set_ylabel('Price [EUR/kWh]')
	ax1.set_xlabel('Time')
	ax1.set_xlim(1,24)
	ax1.legend()
	tikzplotlib.save('./data/output/graphics/%s_WholesalePrices.tex' 
		% (datetime.today().strftime('%Y%m%d')),
			   axis_height = '\\fheight',
			   axis_width = '\\fwidth')
	plt.savefig('./data/output/graphics/%s_WholesalePrices.pdf' 
		% (datetime.today().strftime('%Y%m%d')),
		format='pdf', dpi=1000)


def plot_LocalPrices(PriceBuy,PriceSell,LocalPrice,LocalPrice_expected):	
	t = [x for x in range(1,len(list(PriceBuy.values()))+1)]
    
	fig1 = plt.figure()
	ax1 = fig1.add_subplot(111)
	ax1.plot(t,np.transpose(LocalPrice[0]),'--',color='k',label='Local price')  
	ax1.plot(t,np.transpose(LocalPrice_expected),color='red',
		label='Expected local price') 
	ax1.plot(t,np.transpose(LocalPrice),'--',color='k') 
	ax1.set_ylabel('Price [EUR/kWh]')
	ax1.set_xlabel('Time')
	ax1.set_ylim(min(list(PriceSell.values())),max(list(PriceBuy.values())))
	ax1.set_xlim(1,24)
	ax1.legend()
	tikzplotlib.save('./data/output/graphics/%s_LocalSpotPrice.tex' 
		% (datetime.today().strftime('%Y%m%d')),
			   axis_height = '\\fheight',
			   axis_width = '\\fwidth')
	plt.savefig('./data/output/graphics/%s_LocalSpotPrice.pdf' 
		% (datetime.today().strftime('%Y%m%d')),
		format='pdf', dpi=1000)

		
def plot_inc_var(case,var,payoff,var_lb,var_ub):

	fig1 = plt.figure()
	ax1 = fig1.add_subplot(111)
	#
	ax1.plot(var,payoff['Player1'],'-o',color='green',label='Player1')
	ax1.fill_between(var,var_lb['Player1'], var_ub['Player1'],
		color='green',alpha=0.5,label='var')
	#
	ax1.plot(var,payoff['Player2'],'-^',color='blue',label='Player2')
	ax1.fill_between(var,var_lb['Player2'], var_ub['Player2'],
		color='blue',alpha=0.5,label='var')
	#
	ax1.plot(var,payoff['Player3'],'-d',color='orange',label='Player3')
	ax1.fill_between(var,var_lb['Player3'], var_ub['Player3'],
		color='orange',alpha=0.5,label='var')
	#
	#
	ax1.plot(var,payoff['Player4'],'-*',color='red',label='Player4')
	ax1.fill_between(var,var_lb['Player4'], var_ub['Player4'],
		color='red',alpha=0.5,label='var')	
		
	ax1.set_xlabel('Variance [-]')
	ax1.set_ylabel('Payoffs [EUR]')
	#ax1.set_xlim(max(var),0)
	ax1.legend()
	plt.title(case)
	#
	tikzplotlib.save('./data/output/graphics/%s_Var_%s.tex' 
		% (datetime.today().strftime('%Y%m%d'),case),
			   axis_height = '\\fheight',
			   axis_width = '\\fwidth')
	plt.savefig('./data/output/graphics/%s_Var_%s.pdf' 
		% (datetime.today().strftime('%Y%m%d'),case),
		format='pdf', dpi=1000)		
		
		
def plot_time(Players,startPlayer,stopPlayer,time_spot,time_PSR,time_FSR,time_Coop,time_Shap,time_Nucl):

	t = [x for x in range(startPlayer,stopPlayer+1)]

	fig1 = plt.figure()
	ax1 = fig1.add_subplot(111)
	ax1.plot(t,time_spot,label='spot')
	ax1.plot(t,time_PSR,label='PSR')
	ax1.plot(t,time_FSR,label='FSR')
	ax1.plot(t,[round(sum(x),2) for x in zip(time_Coop, time_Shap)],'--',label='Shap.')
	ax1.plot(t,[round(sum(x),2) for x in zip(time_Coop, time_Nucl)],'--',label='nucl.')

	ax1.set_ylabel('Time [sec]')
	ax1.set_xlabel('Number of players')
	ax1.set_xlim(startPlayer,stopPlayer)
	#

	ax1.legend()
	#
	tikzplotlib.save('./data/output/graphics/%s_RunTime.tex' 
		% (datetime.today().strftime('%Y%m%d')),
				   axis_height = '\\fheight',
				   axis_width = '\\fwidth')
	plt.savefig('./data/output/graphics/%s_RunTime.pdf' 
		% (datetime.today().strftime('%Y%m%d')),
		format='pdf', dpi=1000)