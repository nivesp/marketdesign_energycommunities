import numpy as np
import scipy.stats as stats
from math import erf
import matplotlib.pyplot as plt
from random import uniform

def gen_PVScenarios(PV,NS,b0):
    ''' Generatates dictonary, set and weights of PV power for optimization
	
	Input: Numnber of Scnearios, FullSample, list of players, list of time steps
	Output: Weight of scenarios, list of scenarios, dict of PV power
	example:
	Number_of_Scenarios = 10
	Weight, Scenarios, PowerPV = generate_PVOptData(Number_of_Scenarios,
												FullSample_of_PV,Players,Time)
    '''										
	
    NTime = len(PV)
	
    # Generate mu: 1xk vector of zero
    mu = np.zeros([NTime])
	
    # Generate sigma covarianz matrix which accounts of intertemporal dependence
    # of PV power generation scenarios
    A = np.diag(np.ones(NTime))
    B = np.diag(np.zeros(NTime))+1
    C = B - A
    eta = 7 # strength of correlation
    
    SIGMA = np.diag(np.zeros(NTime))
    for k in range(0,NTime):
        for i in range(0,NTime):
            SIGMA[k,i] =  np.exp(-abs(k-i)/eta)* C[k,i]+A[k,i]
     
    # defining the mean
    mean = PV
                 
    # Variance that accounts for increasing forecast error
    #b0 = 0.1
    b1 = 0.0
    var = np.zeros([NTime])
    for k in range(0,NTime):
            var[k] = b0 + b1*np.sqrt(k)
                           
    # X_k^(s) -> S realizations of X_k
    XKs = np.diag(np.zeros(NTime))
    Xks = np.random.multivariate_normal(mu,SIGMA,NS)
    
    # Y_k^(s)
    aux = np.zeros((len(Xks),len(Xks[0])))
    for s in range(0,len(Xks)):
        for k in range(0,len(Xks[0])):
            aux[s,k] = erf(Xks[s,k]/np.sqrt(2))
            
    Yks = (0.5*(np.ones((len(Xks),len(Xks[0]))) + aux))
    
    # PV power generation scenarios following a beta distribution with alpha 
	# and beta based on mean and variance
    alpha = np.zeros(NTime)
    beta = np.zeros(NTime)
    for k in range(0,NTime):
        alpha[k] = ( mean[k]*( ( ( mean[k]*(1 - mean[k]) )/var[k] ) - 1) )
        beta[k]= ( (1 - mean[k])*( ( ( mean[k]*(1 - mean[k]) )/var[k] ) - 1) )
    
    PV_scenarios = np.zeros((NS,NTime))
    for s in range(0,NS):
        for k in range(0,NTime):
            PV_scenarios[s,k] = stats.beta.ppf(Yks[s,k],alpha[k],beta[k])
    
    # replace all nan in PV_scenarios by 0
    where_are_NaNs = np.isnan(PV_scenarios)
    PV_scenarios[where_are_NaNs] = 0
    
    PV_scenarios = PV_scenarios.round(decimals=2)
    
    # Plot aggregated CDF for each time step
    # Agregated CDF
    F = np.arange(1/(NS+1),1/(NS+1)*NS+1/(NS+1),1/(NS+1))
    
    x = np.zeros((NS,NTime))
    for k in range(0,NTime):
        x[:,k] = np.sort(PV_scenarios[:,k])

    fig1 = plt.figure()
    ax1 = fig1.add_subplot(111)
    ax1.plot(x,F,color='k')   
	
    return PV_scenarios.tolist()


def clustered_PV(FullSample_of_PV,NumberScenarios):
	''' Clusters PV scenarios
	'''
	from sklearn.cluster import KMeans
	
	kmeans = KMeans(n_clusters=NumberScenarios)
	kmeans.fit(FullSample_of_PV)
	labels = kmeans.predict(FullSample_of_PV)
	centroids = kmeans.cluster_centers_.tolist()
	
	return centroids
	
	
def gen_PVInput(Number_of_Scenarios,FullSample_of_PV,Players,Time):	
	''' Generatates dictonary, set and weights of PV power for optimization
	
	Input: Numnber of Scnearios, FullSample, list of players, list of time steps
	Output: Weight of scenarios, list of scenarios, dict of PV power
	example:
	Number_of_Scenarios = 10
	Weight, Scenarios, PowerPV = generate_PVOptData(Number_of_Scenarios,
												FullSample_of_PV,Players,Time)
	'''
	
	Scenarios = ['s%d' % x for x in range(1,Number_of_Scenarios+1)] # scenarios
	weight = 1/len(Scenarios) # weight of each scenario

	# # pv production for player 1
	PV_player = []
	for scenario in FullSample_of_PV[0:len(Scenarios)]:
		PV_playerAUX = [round(6*x,2) for x in scenario]
		PV_player.append(PV_playerAUX)
	
	# dictonary of PV data for all players and scenarios
	PVPower = {}
	for player in Players:
		for scenario in Scenarios:		
			for t in Time:
				if player == 'Player1':
					PVPower.update( {(player,scenario,t) : 
						PV_player[Scenarios.index(scenario)][Time.index(t)] } )
				elif player == 'Player4':
					PVPower.update( {(player,scenario,t) : 
						PV_player[Scenarios.index(scenario)][Time.index(t)] } )	
				elif player == 'Player6':
					PVPower.update( {(player,scenario,t) : 
						uniform(0.8,1.2)*PV_player[Scenarios.index(scenario)][Time.index(t)] } )		
				elif player == 'Player9':
					PVPower.update( {(player,scenario,t) : 
						uniform(0.8,1.2)*PV_player[Scenarios.index(scenario)][Time.index(t)] } )	
				elif player == 'Player10':
					PVPower.update( {(player,scenario,t) : 
						uniform(0.8,1.2)*PV_player[Scenarios.index(scenario)][Time.index(t)] } )	
				elif player == 'Player12':
					PVPower.update( {(player,scenario,t) : 
						uniform(0.8,1.2)*PV_player[Scenarios.index(scenario)][Time.index(t)] } )	
				elif player == 'Player14':
					PVPower.update( {(player,scenario,t) : 
						uniform(0.8,1.2)*PV_player[Scenarios.index(scenario)][Time.index(t)] } )	
				elif player == 'Player16':
					PVPower.update( {(player,scenario,t) : 
						uniform(0.8,1.2)*PV_player[Scenarios.index(scenario)][Time.index(t)] } )			
				else:
					PVPower.update( {(player,scenario,t) : 0} )

	return weight, Scenarios, PVPower