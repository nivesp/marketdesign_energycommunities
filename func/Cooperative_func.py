#!/usr/bin/env python
from itertools import combinations
import math
import bisect
from gurobipy import * # Gurobi

from collections import OrderedDict 
from OptProblem import OptProblem

import re


def atoi(text):
    return int(text) if text.isdigit() else text


def natural_keys(text):
    '''
    alist.sort(key=natural_keys) sorts in human order
    http://nedbatchelder.com/blog/200712/human_sorting.html
    (See Toothy's implementation in the comments)
    '''
    return [ atoi(c) for c in re.split(r'(\d+)', text) ]
	
	

def power_set(List):
    '''
    generates list of all possible coalitions
    '''
    PS = [list(j) for i in range(len(List)) for j in combinations(List, i+1)]
    return PS


def extract(d, keys):
    '''
    extracts the entries vom dict according to coalition
    '''
    return dict((k, d[k]) for k in keys if k in d)	


def shapley(Players,all_coalitions,list_of_values):
    '''
    calculates the shapley value
    '''
    shapley_values = {}
    n = len(Players)
    for i in Players:        
        shapley = 0
        for j in all_coalitions:
            if i not in j:
                cmod = len(j)
                Cui = j[:]
                bisect.insort_left(Cui,i)
                Cui.sort(key=natural_keys) # needed for natural sorted
                l = all_coalitions.index(j)
                k = all_coalitions.index(Cui)
                temp = ( list_of_values[k] - list_of_values[l] ) *\
                           float(math.factorial(cmod) * math.factorial(n - cmod - 1)) / float(math.factorial(n))
                shapley += temp
                # if i is 0:
                #     print j, Cui, cmod, n-cmod-1, characteristic_function[k], characteristic_function[l], math.factorial(cmod), math.factorial(n - cmod - 1), math.factorial(n)

        cmod = 0
        Cui = [i]
        k = all_coalitions.index(Cui)
        temp = list_of_values[k] * float(math.factorial(cmod) * math.factorial(n - cmod - 1)) / float(math.factorial(n))
        shapley += temp

        shapley_values_AUX = {i : shapley} 
        shapley_values.update(shapley_values_AUX)

    return shapley_values


def nucleolus(Players,all_coalitions,Value):
    '''
    calculates the maximum excess among all potential coalitions
    '''	
    # 1. iteration
    m = Model('nucl_LP1')
    m.Params.OptimalityTol = 1e-06
    m.Params.FeasibilityTol = 1e-06 
    m.Params.BarConvTol = 0.01 # optimality tolerance
    m.Params.method = 2
    # Sets
    Players = Players
    Coalitions = [tuple(x) for x in all_coalitions]
    # Parameters
    Value = Value
    Value_grand = Value[tuple(Players)] 
    # Variables
    excess_max = m.addVar(ub=0, lb=-1e06, name="excess_max")
    nucl_alloc = m.addVars(Players, lb=0, name="nucl_alloc")

    # create objective
    Obj = m.setObjective(excess_max, GRB.MINIMIZE)

    # create constraints
    EQ_balance = m.addConstr( quicksum(nucl_alloc[n] for n in Players) == Value_grand )

    EQ_maxExcess = m.addConstrs( ( Value[tuple(s)]  
                                - quicksum(nucl_alloc[n] for n in s)
                                <= excess_max ) for s in Coalitions )

    # solve
    m.update()
    m.optimize()
    if m.status == GRB.Status.OPTIMAL:
        print('\n Iteration 1 \n OPTIMAL SOLUTION FOUND \n')
    else:
        sys.exit('\n ERROR: NO SOLUTION FOUND \n')

    nucl_alloc_1 = m.getAttr('x',nucl_alloc)
    excess_max_1 = excess_max.x

    # calculate binding coalitions
    Sigma_b_1 = []
    Sigma_nb_1 = []
    for s in Coalitions:
        if EQ_maxExcess[s].pi != 0.0:
            Sigma_b_1.append(s)
        else:
            Sigma_nb_1.append(s) 

    # Safe data
    Nucl_alloc_Out = {1 : nucl_alloc_1}
    Excess_max_Out = {1 : excess_max_1}
    Sigma_b = {1 : Sigma_b_1}
    Sigma_nb = {1 : Sigma_nb_1}
    
    # Start iteration
    #################
    l = 0
    for l in range(2,1000):
        m = Model('nucl_LP' + str(l))
        m.Params.OptimalityTol = 1e-06
        m.Params.FeasibilityTol = 1e-06 
        m.Params.BarConvTol = 0.01 # optimality tolerance
        m.Params.method = 2
        # Sets
        Players = Players
        Coalitions = [tuple(x) for x in all_coalitions]
        # Parameters
        Value = Value
        Value_grand = Value[tuple(Players)] 
        # Variables
        excess_max = m.addVar(ub=0, lb=-1e06, name="excess_max")
        nucl_alloc = m.addVars(Players, lb=0, name="nucl_alloc")

        # create objective
        Obj = m.setObjective(excess_max, GRB.MINIMIZE)

        # create constraints
        EQ_balance = m.addConstr( quicksum(nucl_alloc[n] for n in Players) == Value_grand )
        
        # where zz goes from 1 to l-1
        for zz in range(1,l):
            EQ_maxExcess_b = m.addConstrs( quicksum(nucl_alloc[n] for n in s)
                                        == Value[s] - Excess_max_Out[zz] 
                                        for s in Sigma_b[zz] )
            
        if l-1 in Sigma_nb:
            EQ_maxExcess_nb = m.addConstrs( ( Value[s]  
                                        - quicksum(nucl_alloc[n] for n in s)
                                        <= excess_max )
                                        for s in Sigma_nb[l-1] )

        # solve
        m.update()
        m.write('Nucl_alloc.lp' )
        m.optimize()
        if m.status == GRB.Status.OPTIMAL:
            print('\n Iteration ' + str(l) + '\n OPTIMAL SOLUTION FOUND \n')
        else:
            sys.exit('\n ERROR: NO SOLUTION FOUND \n')

        # update
        nucl_alloc = m.getAttr('x',nucl_alloc)
        excess_max = excess_max.x
        
        Nucl_alloc_Out.update({l : nucl_alloc})
        Excess_max_Out.update({l : excess_max})
        
        # calculate binding coalitions
        Sigma_b_AUX = []
        Sigma_nb_AUX = []
        if l-1 in Sigma_nb:
            for s in Sigma_nb[l-1]:
                if EQ_maxExcess_nb[s].pi != 0.0:    
                    Sigma_b_AUX.append(s)
                    Sigma_b.update({l : Sigma_b_AUX})
                else:
                    Sigma_nb_AUX.append(s)
                    Sigma_nb.update({l : Sigma_nb_AUX})  
        
        # check convergence criterion
        if l-1 not in Sigma_nb:
            break
    print('Nucleolus allocation found \n ' + str(nucl_alloc))

    return nucl_alloc


def calc_Nucl(s,Players,all_coalitions,value_per_scenario):

	dict_of_values = { tuple(coalition) : value_per_scenario[tuple(coalition)][s] for coalition in all_coalitions }
	nucl_per_scenario = OrderedDict({s : nucleolus(Players, all_coalitions, dict_of_values)})

	return nucl_per_scenario

	
	
def calc_Coop(coalition,Coop_Input_AUX,model_name,Coop_Settings):

	Coop_Input = 	{'Players' : 	coalition,
					#
					 'Scenarios' : 	Coop_Input_AUX['Scenarios'],
					 'Time' : 		Coop_Input_AUX['Time'],
					 'PriceBuy' : 	Coop_Input_AUX['PriceBuy'],
					 'PriceSell' : 	Coop_Input_AUX['PriceSell'],
					 'Weight' : 	Coop_Input_AUX['Weight'],
					#
					 'PowerPV' : 	extract(Coop_Input_AUX['PowerPV'], [(n,e,t) 
										for n in coalition 
										for e in Coop_Input_AUX['Scenarios'] 
										for t in Coop_Input_AUX['Time']]),
					#
					 'Demand' : 	extract(Coop_Input_AUX['Demand'], [(n,t) 
										for n in coalition
										for t in Coop_Input_AUX['Time']]),
					#
					 'EffCh' : 		Coop_Input_AUX['EffCh'],
					 'EffDis' : 	Coop_Input_AUX['EffDis'],
					 'SocIni' : 	Coop_Input_AUX['SocIni'],
					 'Storage' : 	Coop_Input_AUX['Storage'],
					 'StorageLoc' : Coop_Input_AUX['StorageLoc'],
					 'ChMax' : 		Coop_Input_AUX['ChMax'],
					 'DisMax' : 	Coop_Input_AUX['DisMax'],
					 'SocMax' : 	Coop_Input_AUX['SocMax']}  

	# Initialize model and run Optimization							
	Coop = OptProblem(model_name,Coop_Input,Coop_Settings) 
	Coop.run_opt()
	Coop.get_results(Coop_Settings)
	
	# expected
	cost_of_coalition_expected = {tuple(coalition) : Coop.m.objVal}
	
	# per scenario
	cost_of_coalition_per_scenario = {tuple(coalition) : OrderedDict({e : sum( Coop.PriceBuy[t]*Coop.powerBuy[e,t] - Coop.PriceSell[t]*Coop.powerSell[e,t] for t in Coop.Time ) for e in Coop_Input_AUX['Scenarios']}) }	
	
	return cost_of_coalition_expected, cost_of_coalition_per_scenario
