''' 
=========================================================================
Script         : Input data
Author         : Niklas Vepermann
Project        : Risk trading in energy communities
=========================================================================
'''

# Load packages
import numpy as np

from datetime import datetime
import pandas as pd    # Pandas


def data_to_Gams(InputData):
	import pandas as pd
	import gdxpds 
	
	# Sets
	nn = pd.DataFrame( InputData['Players'] )
	nn['value'] = True
	nn = nn.rename(index=str, columns={'n':'set'})

	ww = pd.DataFrame( InputData['Scenarios'] )
	ww['value'] = True
	ww = ww.rename(index=str, columns={'w':'set'})
	
	tt = pd.DataFrame( InputData['Time'] )
	tt['value'] = True
	tt = tt.rename(index=str, columns={'t':'set'})

	# Parameters
	PriceBuy = pd.Series(InputData['PriceBuy']).rename_axis(['t']
		).reset_index(name='value')

	PriceSell = pd.Series(InputData['PriceSell']).rename_axis(['t']
		).reset_index(name='value')

	Weight = pd.Series(InputData['Weight']).rename_axis(['w']
		).reset_index(name='value')

	PowerPV = pd.Series(InputData['PowerPV']).rename_axis(['n', 'w', 't']
		).reset_index(name='value')
	PowerPV = PowerPV[['n', 't', 'w', 'value']]
	
	Demand = pd.Series(InputData['Demand']).rename_axis(['n', 't']
		).reset_index(name='value')
	
	EffCh = pd.Series(InputData['EffCh']).rename_axis(['n']
		).reset_index(name='value')
	
	EffDis = pd.Series(InputData['EffDis']).rename_axis(['n']
		).reset_index(name='value')
	
	SocIni = pd.Series(InputData['SocIni']).rename_axis(['n']
		).reset_index(name='value')
	
	ChMax = pd.Series(InputData['ChMax']).rename_axis(['n']
		).reset_index(name='value')
	
	DisMax = pd.Series(InputData['DisMax']).rename_axis(['n']
		).reset_index(name='value')
	
	SocMax = pd.Series(InputData['SocMax']).rename_axis(['n']
		).reset_index(name='value')

	# assume we have a DataFrame df with last column 'value'
	data_ready_for_GAMS = { 'n': nn,
							'w': ww,
							't': tt,
							#
						 'PriceBuy' : PriceBuy,
						 'PriceSell' : PriceSell,
						 'Weight' : Weight,
						 'PowerPV' : PowerPV,
						 'Demand' : Demand,
						 'EffCh' : EffCh,
						 'EffDis' : EffDis,
						 'SocIni' : SocIni,
						 'ChMax' : ChMax,
						 'DisMax' : DisMax,
						 'SocMax' : SocMax
						 } 

	gdx_file = '.\data\data_to_gams.gdx'
	gdx = gdxpds.to_gdx(data_ready_for_GAMS, gdx_file)



# def gams_payoff_to_df(results_GAMS,Scenarios,Players):
	# # Payoff per scenario
	# #####################
	# Number_of_Scenarios = len(Scenarios)
	# Payoff_per_scenario = pd.DataFrame({}, index=Scenarios)

	# Payoff_per_scenario['Arb'] = ( 
		# results_GAMS['Payoff_per_scenario_Arb']['Value'].tolist() )

	# nn = 0
	# for n in Players:
		# Payoff_per_scenario[n] = ( 
			# results_GAMS['Payoff_per_scenario_Player']['Value'][nn:nn+Number_of_Scenarios].tolist() )
		# nn = nn + Number_of_Scenarios

	# # total payoff per scenario
	# Payoff_per_scenario['Tot'] = (
		# Payoff_per_scenario.sum(axis=1) )

	# # Payoff risk adjusted expected
	# ###############################
	# Payoff_expected = pd.DataFrame(
		# {'Arb' : [0] }, index=['Payoff'] )

	# Payoff_expected.loc['Payoff','Arb'] = (
		# results_GAMS['Payoff_expected_Arb']['Value'].tolist()[0] )

	# nn = 0
	# for n in Players:
		# Payoff_expected.loc['Payoff',n] = (
			# results_GAMS['Payoff_expected_Player']['Value'][nn:nn+1].tolist()[0] )
		# nn = nn + 1

	# # total risk adjusted expected payoff
	# Payoff_expected.loc['Payoff','Tot'] = (
		# Payoff_expected.sum(axis=1)['Payoff'] )	

	# return Payoff_per_scenario, Payoff_expected
